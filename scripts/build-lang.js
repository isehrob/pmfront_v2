import * as fs from 'fs';
import path from 'path';
import {sync as globSync} from 'glob';
import {sync as mkdirpSync} from 'mkdirp';


process.env.BABEL_ENV = "development";

const MESSAGES_PATTERN = './src/messages/**/*.json';
const TRANSLATIONS_DIR         = './src/translations/';

// Aggregates the default messages that were extracted from the example app's
// React components via the React Intl Babel plugin. An error will be thrown if
// there are messages in different components that use the same `id`. The result
// is a flat collection of `id: message` pairs for the app's default locale.
let defaultMessages = globSync(MESSAGES_PATTERN)
    .map((filename) => fs.readFileSync(filename, 'utf8'))
    .map((file) => JSON.parse(file))
    .reduce((collection, descriptors) => {
        descriptors.forEach(({id, defaultMessage}) => {
            if (collection.hasOwnProperty(id)) {
                throw new Error(`Duplicate message id: ${id}`);
            }

            collection[id] = defaultMessage;
        });

        return collection;
    }, {});

mkdirpSync(TRANSLATIONS_DIR);
// don't know how to handle no file exceptions yet
// just will assume that theres a  translation files
const updateMessages = (langCode, updatedMessages) => {
  const filePath = path.join(TRANSLATIONS_DIR, `${langCode}.json`);
  let contents = fs.readFileSync(filePath);
  contents = JSON.parse(contents);
  let newMessages = Object.keys(updatedMessages).reduce((collection, key) => {
      if (
          contents.hasOwnProperty(key) &&
          contents[key].trim().length !== 0
         )
      {
        collection[key] = contents[key];
      } else {
        collection[key] = updatedMessages[key];
      }
      return collection;
  }, {});
  fs.writeFileSync(filePath, JSON.stringify(newMessages, null, 2));
}

updateMessages('en', defaultMessages);
updateMessages('fr', defaultMessages);
updateMessages('ru', defaultMessages);


// fs.writeFileSync(TRANSLATIONS_DIR + 'en.json', JSON.stringify(defaultMessages, null, 2));
// fs.writeFileSync(TRANSLATIONS_DIR + 'fr.json', JSON.stringify(defaultMessages, null, 2));
// fs.writeFileSync(TRANSLATIONS_DIR + 'ru.json', JSON.stringify(defaultMessages, null, 2));
