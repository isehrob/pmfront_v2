import {fromJS} from 'immutable';
import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

import {routeMiddleware} from './router';

const sagaMiddleware = createSagaMiddleware();

const logger = store => next => action => {
  console.group(action.type);
  console.info('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState().toJS());
  console.groupEnd(action.type);
  return result;
}

export default function configureStore(reducers, rootSagas) {
  // Create the store with middlewares
  // sagaMiddleware: Makes redux-sagas work
  const middlewares = [
    sagaMiddleware,
    logger,
    routeMiddleware
  ];

  const store = createStore(
    reducers,
    fromJS({}), // initialState
    applyMiddleware(...middlewares)
  );

  rootSagas.map(rootSaga => sagaMiddleware.run(rootSaga));
  return store;
}
