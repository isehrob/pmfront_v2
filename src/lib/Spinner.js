import React from 'react';

import './spinner.css';

export const Spinner = (
  {height, width} = {height: 'inherit', width: 'inherit'}
) => (
  <span>
    <div className="spinnerContainer" style={{height, width}}/>
    <div className="spinnerClass"></div>
  </span>
);


export const withSpinner = loadingProp => (WrappedComponent) => {
  return class extends React.Component {
    render(){
      const { loading } = this.props[loadingProp];
      return (
        <div>
          {loading ? <Spinner /> : null}
          <WrappedComponent {...this.props} />
        </div>
      );
    }
  }
}

export default Spinner;
