import request from 'superagent';
import { delay } from 'Common/utils';
import { NEW_API_BASE_URL } from 'Common/constants';
import getTranslations from 'Lib/get-translations';

export const loggerServiceFake = (error, info, cb) => {
  return delay(500).then(
    () => console.log("LOGGING: "+error, info)
  );
}

export const loggerServiceReal = (error, info) => {
  const url = `${NEW_API_BASE_URL}/logger/log-js`;
  const formVal = {error, info}
  formVal._csrf = window._csrf;
  formVal.content = JSON.stringify({error, info})
  formVal.type = 'reactjs-error';
  const req = request
    .post(url)
    .type("form")
    .send(formVal)
  return req.end();
}

export const translationServiceFake = () => {
  return getTranslations();
}

export const translationServiceReal = () => {
  if(window.REACT_TRANSLATIONS)
    return window.REACT_TRANSLATIONS;
  return getTranslations();
}

export const geolocationServiceFake = () => {
  return delay(500).then(
    () => ({'body': JSON.stringify({
      	"country_code":"UZ",
      	"country_name":"Uzbekistan",
      	"city":"null",
      	"postal":null,
      	"latitude":41,
      	"longitude":64,
      	"IPv4":"91.212.89.45",
      	"state":"null"
      })})
    );
}

export const geolocationServiceReal = () => {
  const url = `${NEW_API_BASE_URL}/utils/geolocation`;
  const req = request.get(url);
  return req;
}

let translationService;
let loggerService;
let geolocationService;

if(process.env.NODE_ENV === "production") {
  loggerService = loggerServiceReal;
  translationService = translationServiceReal;
  geolocationService = geolocationServiceReal;
} else {
  loggerService = loggerServiceFake;
  translationService = translationServiceFake;
  geolocationService = geolocationServiceFake;
}

export default {
  loggerService,
  translationService,
  geolocationService
};
