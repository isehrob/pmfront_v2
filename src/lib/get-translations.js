import enTranslations from '../translations/en.json';
import ruTranslations from '../translations/ru.json';
import frTranslations from '../translations/fr.json';

import { LOCALE } from 'Common/constants';

export default () => {
  switch (LOCALE) {
    case "ru":
      return ruTranslations;
    case "fr":
      return frTranslations;
    default:
      return enTranslations;
  }
}
