import React from 'react';
import shallowCompare from 'react-addons-shallow-compare';

// HOOC for handle scrollTo event
export const withScrollTo = (
  // `full` is used to determine wether to use or not scroll in new props receive
  // in some cases `componentWillReceiveProps` very rapidly and many times
  // which freezes the window for some time
  DOMNodeId, full = true
) => (Wrapped) =>
  class extends React.Component {
    handleScroll = () => {
      const targetNode = document && document.getElementById(DOMNodeId);
      const scrollTo = targetNode && targetNode.offsetTop;
      const $ = window.jQuery;
      if ($) {
        scrollTo && $('html,body').animate({scrollTop: scrollTo});
      } else {
        scrollTo && window.scrollTo(0, scrollTo);
      }
    }
    componentDidMount(){
      this.handleScroll();
    }
    componentWillReceiveProps(nextProps, nextState){
      if(shallowCompare(this, nextProps, nextState) && full){
        this.handleScroll();
      }
    }
    render(){
      return <Wrapped {...this.props} />
    }
  }

// TODO: Useless component, need to remove it
export const withTopOffsetInjected = (Wrapped) =>
  class extends React.Component {
    componentDidMount(){
      // TODO: verry strange things happening here need to fix
      // I'm not thinking right know
      // I just want this all work whatever it takes!
      this.offsetTop = this.el.offsetTop;
      this.forceUpdate();
    }
    render(){
      return (
        <div className="clearfix" ref={(el) => this.el = el}>
          <Wrapped {...this.props} offsetTop={this.offsetTop} />
        </div>
      );
    }
  }
