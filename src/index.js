// Needed for redux-saga es6 generator support
import 'babel-polyfill';
import 'core-js';

import React from 'react';
import ReactDOM from 'react-dom';
import {combineReducers} from 'redux-immutable';
import {reducer as formReducer} from 'redux-form/immutable';
// router
import {routerReducer} from 'react-router-redux';

import configureStore from './store';

import Qa from './apps/qa';
import qaReducer from './apps/qa/reducer';
import qaSaga from './apps/qa/sagas';

import Authorities from './apps/authorities';
import authoritiesReducer from './apps/authorities/reducer';
import authoritiesSaga from './apps/authorities/sagas';

import Receptions from './apps/receptions';
import receptionsReducer from './apps/receptions/reducer';
import receptionsSaga from './apps/receptions/sagas';

import Appeal from './apps/appeal';

import Consulates from './apps/consulates';
import consulatesReducer from './apps/consulates/reducer';
import consulatesSaga from './apps/consulates/sagas';

import ErrorHandler from './common/error-handler';

// configure react-intl
import { addLocaleData } from 'react-intl';
import en from 'react-intl/locale-data/en'; // for uzbek cyrillic
import fr from 'react-intl/locale-data/fr'; // for uzbek latin
import ru from 'react-intl/locale-data/ru'; // for russian

addLocaleData([...en, ...ru, ...fr]);


const rootReducer = combineReducers({
  qa: qaReducer,
  authorities: authoritiesReducer,
  receptions: receptionsReducer,
  consulates: consulatesReducer,
  form: formReducer,
  router: routerReducer
});

const rootSaga = [qaSaga, authoritiesSaga, receptionsSaga, consulatesSaga];

export const store = configureStore(rootReducer, rootSaga);

const renderApps = () => {
  ReactDOM.render(
    <ErrorHandler appLabel='qa_app'>
      <Qa store={store} />
    </ErrorHandler>,
    document.getElementById('qa_app_root')
  );

  ReactDOM.render(
    <ErrorHandler appLabel='rc_app'>
      <Authorities store={store} />
    </ErrorHandler>,
    document.getElementById('rc_app_root')
  );

  ReactDOM.render(
    <ErrorHandler appLabel='map_app'>
      <Receptions store={store} />
    </ErrorHandler>,
    document.getElementById('map_app_root')
  );

  ReactDOM.render(
    <Appeal store={store} />,
    document.getElementById('appeal_app_root')
  );


  ReactDOM.render(
    <Consulates store={store} />,
    document.getElementById('consulates_app_root')
  );
}

// If browser doesn't support Intl (i.e. Safari), then we manually import
// the intl polyfill and locale data.
if (!window.intl) {
  require.ensure([
    'intl',
    'intl/locale-data/jsonp/en.js',
    'intl/locale-data/jsonp/fr.js',
    'intl/locale-data/jsonp/ru.js',
  ], (require) => {
    require('intl');
    require('intl/locale-data/jsonp/en.js');
    require('intl/locale-data/jsonp/fr.js');
    require('intl/locale-data/jsonp/ru.js');
    renderApps();
  });
} else {
  renderApps();
}
