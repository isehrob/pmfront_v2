/*
 *
 * App reducers
 *
 */

import {fromJS} from 'immutable';

import {
  LOAD_DATA,
  CANCEL_LOAD,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_FAIL,
  STATE_FORWARD,
  STATE_BACKWARD,
  STATE_TO,
} from './constants';
import nodes from './stateNodes';

const initialState = fromJS({
  data: undefined,
  success: true,
  loading: false,
  msg: false,
  stateStack: [], // implementing appstate as a stack
});

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_DATA:
      return state.set('loading', true);
    case LOAD_DATA_SUCCESS:
      return state.set('data', action.data)
                  .set('loading', false)
                  .set('success', true);
    case LOAD_DATA_FAIL:
      return state.set('loading', false)
                  .set('data', undefined)
                  .set('success', false);
    case STATE_TO:
      let stateStack = state.get('stateStack').toJS();
      // find desired node by it's key, assign
      // params given to it by an action and
      // push it to the state stack
      let nextState = nodes[action.stateTo];
      nextState.params = action.params;
      // temporary solution to prevent the state stack
      // to be filled with obsolete data
      if (nextState.id === "locations") stateStack = [];
      stateStack.unshift(nextState);
      return state.set('loading', true)
                  .set('stateStack', fromJS(stateStack));
    case STATE_FORWARD:
      // here we get the current node in the stack
      // and through it's next prop get the next node
      // and put in to the stack, so the apps current
      // state change to it. At the same time we give
      // next node params given to it by an action
      stateStack = state.get('stateStack').toJS();
      let currentState = stateStack[0];
      nextState = nodes[currentState.next];
      nextState.params = action.params
      stateStack.unshift(nextState);
      return state.set("loading", true)
                  .set('stateStack', fromJS(stateStack));
    case STATE_BACKWARD:
      // just pop an element from the stack and return it
      stateStack = state.get('stateStack').toJS();
      stateStack.shift();
      return state.set("loading", true)
                  .set('stateStack', fromJS(stateStack));
    case CANCEL_LOAD:
      return state.set("loading", false);
    default:
      return state;
  }
}
