/*
 *
 * LocationApp actions
 *
 */

import {
  LOAD_DATA,
  CANCEL_LOAD,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_FAIL,
  LOAD_REGIONS,
  LOAD_LOCATIONS,
  LOAD_DETAIL,
  STATE_FORWARD,
  STATE_BACKWARD,
  STATE_TO,
  SEARCH_REQUEST
} from './constants';

export const loadData = () => ({
  type: LOAD_DATA
});

export const cancelLoad = () => ({
  type: CANCEL_LOAD
});

export const loadDataSuccess = (resp) => ({
  type: LOAD_DATA_SUCCESS,
  data: resp
});

export const loadDataFail = (err) => ({
  type: LOAD_DATA_FAIL,
  msg: err.message
});

export const goTo = (params) => {
  const { stateTo, ...rest } = params;
  return {
    type: STATE_TO,
    params: rest,
    stateTo
  };
}

export const goForward = (params) => ({
  type: STATE_FORWARD,
  params
});

export const goBackward = () => ({
  type: STATE_BACKWARD
});

export const loadRegions = () => ({
  type: LOAD_REGIONS
});

export const loadLocations = (regionId) => ({
  type: LOAD_LOCATIONS,
  regionId
});

export const loadDetail = (id) => ({
  type: LOAD_DETAIL,
  id
});

export const searchRequest = (searchTerm) => ({
  type: SEARCH_REQUEST,
  searchTerm
});
