export const regions = [
  {"id":11,"order":400,"title":"Фарғона вилояти", map_id: "jqvmap1_fa", "count_reception": 20},
	{"id":6,"order":400,"title":"Наманган вилояти", map_id: "jqvmap1_no", "count_reception": 13},
	{"id":2,"order":400,"title":"Бухоро вилояти", map_id: "jqvmap1_bu", "count_reception": 14},
	{"id":4,"order":400,"title":"Қашқадарё вилояти", map_id: "jqvmap1_qa", "count_reception": 15},
	{"id":5,"order":400,"title":"Навоий вилояти", map_id: "jqvmap1_na", "count_reception": 11},
	{"id":8,"order":400,"title":"Сурхондарё вилояти", map_id: "jqvmap1_te", "count_reception": 15},
	{"id":9,"order":400,"title":"Сирдарё вилояти", map_id: "jqvmap1_gu", "count_reception": 12},
	{"id":10,"order":400,"title":"Тошкент вилояти", map_id: "jqvmap1_to", "count_reception": 19},
	{"id":12,"order":400,"title":"Хоразм вилояти", map_id: "jqvmap1_xo", "count_reception": 12},
	{"id":14,"order":450,"title":"Тошкент шаҳри", map_id: "jqvmap1_tosh", "count_reception": 12},
	{"id":1,"order":400,"title":"Андижон вилояти", map_id: "jqvmap1_an", "count_reception": 17},
	{"id":3,"order":400,"title":"Жиззах вилояти", map_id: "jqvmap1_ji", "count_reception": 14},
	{"id":13,"order":300,"title":"Қорақалпоғистон Республикаси", map_id: "jqvmap1_qo", "count_reception": 16},
	{"id":7,"order":400,"title":"Самарқанд вилояти", map_id: "jqvmap1_sa", "count_reception": 17}
];

export const locations = [
  {"id":2,"order":500,"title":"Андижон вилояти халқ қабулхонаси","phone":"0 (374) 223-10-00 ","email":"xq_andijonviloyat@umail.uz","fax":"","district_id":29,"district_title":"Андижон шаҳар","address":"Андижон шаҳри, Миллий тикланиш кўчаси, 24 уй\\r\\n"},
  {"id":93,"order":500,"title":"Андижон шаҳар халқ қабулхонаси","phone":"0 (374) 228-10-00","email":"xq_andijonshahar@umail.uz","fax":"","district_id":29,"district_title":"Андижон шаҳар","address":"Амир Темур кўчаси, 11 уй\\r\\n"},
  {"id":96,"order":500,"title":"Хонобод шаҳар халқ қабулхонаси","phone":"0 (374) 734-10-00","email":"xq_xonobodshahar@umail.uz","fax":"","district_id":30,"district_title":"Хонобод шаҳар","address":"Шифобахш кўчаси, 5 уй\\r\\n"},
  {"id":99,"order":500,"title":"Андижон тумани халқ қабулхонаси","phone":"0 (374) 373-10-00","email":"xq_andijontuman@umail.uz","fax":"","district_id":16,"district_title":"Андижон тумани","address":"Куйганёр ШФЙ, Ижодкор кўчаси, 1 уй\\r\\n"},
  {"id":100,"order":500,"title":"Асака тумани халқ қабулхонаси","phone":"0 (374) 233-10-00","email":"xq_asakatuman@umail.uz","fax":"","district_id":17,"district_title":"Асака тумани","address":"И.Бухорий кўчаси, 8 уй"},
  {"id":101,"order":500,"title":"Балиқчи тумани халқ қабулхонаси","phone":"0 (374) 323-10-00","email":"xq_baliqchituman@umail.uz","fax":"","district_id":18,"district_title":"Балиқчи тумани","address":"Балиқчи тумани, Балиқчи шох кўчаси, 39-уй"},
  {"id":102,"order":500,"title":"Булоқбоши тумани халқ қабулхонаси","phone":"0 (374) 773-10-00","email":"xq_buloqboshituman@umail.uz","fax":"","district_id":20,"district_title":"Булоқбоши тумани","address":"Булоқбоши тумани, М.Исмоилий кўчаси, 10-уй"},
  {"id":104,"order":500,"title":"Бўз тумани халқ қабулхонаси","phone":"0 (374) 333-10-00","email":"xq_boztuman@umail.uz","fax":"","district_id":19,"district_title":"Бўз тумани","address":"Бўз тумани, Улуғбек кўчаси, 1-уй\\r\\n"},
  {"id":108,"order":500,"title":"Жалақудуқ тумани халқ қабулхонаси","phone":"0 (374) 755-10-00","email":"xq_jalolquduqtuman@umail.uz","fax":"","district_id":21,"district_title":"Жалақудуқ тумани","address":"Жалақудуқ тумани, Ўзбекистон кўчаси, 18-уй"},
  {"id":113,"order":500,"title":"Избоскан тумани халқ қабулхонаси","phone":"0 (374) 312-10-00","email":"xq_izboskantuman@umail.uz","fax":"","district_id":22,"district_title":"Избоскан тумани","address":"Избоскан тумани, Пойтуғ шаҳри, Нақшбанд кўчаси, 22-уй"},
  {"id":117,"order":500,"title":"Мархамат тумани халқ қабулхонаси","phone":"0 (374) 391-10-00","email":"0 (374) 391-29-04","fax":"","district_id":24,"district_title":"Марҳамат тумани","address":"Марҳамат тумани, Марҳамат шаҳри, Мустақиллик кўчаси, 114-уй"},
  {"id":118,"order":500,"title":"Олтинкўл тумани халқ қабулхонаси","phone":"0 (374) 354-10-00","email":"xq_oltinkoltuman@umail.uz","fax":"0 (374) 354-58-18","district_id":15,"district_title":"Олтинкўл тумани","address":"Олтинкўл тумани, Туркистон кўчаси, 6-уй"},
  {"id":119,"order":500,"title":"Пахтаобод тумани халқ қабулхонаси","phone":"0(374) 711-10-00","email":"xq_pahtaobodtuman@umail.uz","fax":"0(374) 711-11-87","district_id":25,"district_title":"Пахтаобод тумани","address":"Пахатобод тумани, Пахтаобод шаҳри, Амир Темур кўчаси, 2-уй"},
  {"id":120,"order":500,"title":"Улуғнор тумани халқ қабулхонаси","phone":"0(374) 761-10-00","email":"xq_ulugnortuman@umail.uz","fax":"0(374) 761-17-00","district_id":26,"district_title":"Улуғнор тумани","address":"Улуғнор тумани, Оқ олтин қишлоғи, Мустақиллик кўчаси, 3-уй "},
  {"id":125,"order":500,"title":"Хўжаобод тумани халқ қабулхонаси","phone":"0(374) 741-10-00","email":"xq_xujaobodtuman@umail.uz","fax":"0(374) 741-16-00","district_id":17,"district_title":"Асака тумани","address":"Хўжаобод тумани, Узун кўчаси, 27-уй"},
  {"id":129,"order":500,"title":"Шахрихон тумани халқ қабулхонаси","phone":"0(374) 342-10-00","email":"xq_shahrixontuman@umail.uz","fax":"","district_id":28,"district_title":"Шахрихон тумани","address":"Шахрихон тумани, Р.Ёдгоров кўчаси, 5-уй"},
  {"id":132,"order":500,"title":"Қўрғонтепа тумани халқ қабулхонаси","phone":"0(374) 723-10-00","email":"xq_qurgontepatuman@umail.uz","fax":"","district_id":23,"district_title":"Қўрғонтепа тумани","address":"Кўрғонтепа тумани, Андижон кўчаси, 5-уй"}
];

export const details = {
	"id":2,
	"title":"Андижон вилояти халқ қабулхонаси",
	"address":"Андижон шаҳри, Миллий тикланиш кўчаси, 24 уй\r\n",
	"phone":"0 (374) 223-10-00 ",
	"email":"xq_andijonviloyat@umail.uz",
	"fax":"",
	"longitude":"72.354412",
	"latitude":"40.762181",
	"zoom":"7",
	"region_id":1,
	"category_title":"Андижон вилояти",
	"district_id":29,
	"district_title":"Андижон шаҳар"
};

export const searchResults = [
	{"id":22,"reception_title":"Quvasoy shahar xalq qabulxonasi","region_id":11,"region_title":"Farg‘ona viloyati","map_id":null},
	{"id":29,"reception_title":"Buvayda tumani xalq qabulxonasi","region_id":11,"region_title":"Farg‘ona viloyati","map_id":null},
	{"id":38,"reception_title":"Quva tumani xalq qabulxonasi","region_id":11,"region_title":"Farg‘ona viloyati","map_id":null}
];
