import { /*take,*/ call, put, /*cancel,*/ all, takeLatest } from 'redux-saga/effects';
// import { LOCATION_CHANGE } from 'react-router-redux';
import getApi from './api';
import {
  loadDataSuccess,
  loadDataFail,
} from './actions';
import {
  LOAD_REGIONS,
  LOAD_LOCATIONS,
  LOAD_DETAIL,
  SEARCH_REQUEST
} from './constants';

const Api = getApi();

// Individual exports for testing
export function* loadRegionsSaga() {
  try {
    const resp = yield call(Api.loadRegions);
    yield put(loadDataSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadLocationsSaga(action){
  try {
    const resp = yield call(Api.loadLocations, action.regionId);
    yield put(loadDataSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadDetailSaga(action){
  try {
    const resp = yield call(Api.loadDetail, action.id);
    yield put(loadDataSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* searchRequestSaga(action){
  try{
    const resp = yield call(Api.search, action.searchTerm);
    yield put(loadDataSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* watchLoadRegions() {
  yield takeLatest(LOAD_REGIONS, loadRegionsSaga);
}

// load sphere item saga
export function* watchLoadLocations(){
  yield takeLatest(LOAD_LOCATIONS, loadLocationsSaga);
}

// load sphere item saga
export function* watchLoadDetail(){
  yield takeLatest(LOAD_DETAIL, loadDetailSaga);
}

export function* watchSearchRequestSaga(){
  yield takeLatest(SEARCH_REQUEST, searchRequestSaga);
}

// All sagas to be loaded
export default function* qaRootSaga() {
  yield all([
    watchLoadRegions(),
    watchLoadLocations(),
    watchLoadDetail(),
    watchSearchRequestSaga()
  ]);
}
