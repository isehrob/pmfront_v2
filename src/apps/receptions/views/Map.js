import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';
import mapData from '../mapData';
import { Api } from '../api';
import LocationsList from './LocationsList';
import List from './List';


const MapDisplay = ({
  handleMouseOver,
  handleMouseLeave,
  handleClick,
  hoverRegion,
  selectedRegion,
  regions
}) => {
  return (
    <div id="vmap">
        <svg width="600" height="400">
            <g transform="scale(1) translate(0, 0)">
              {regions.map((data, i) => (
                <path d={mapData[data.map_id]}
                  key={i}
                  onMouseOver={() => handleMouseOver(data)}
                  onMouseLeave={handleMouseLeave}
                  onClick={() => handleClick(data)}
                  stroke="#fff"
                  strokeWidth="1"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeOpacity="1"
                  fillOpacity={
                    (hoverRegion && (hoverRegion.id === data.id) && ".8") || "1"
                  }
                  fill={
                      (selectedRegion &&
                        (selectedRegion.id === data.id) &&
                        "#81c67a") || "#00b7fb"
                    }
                  id={data.mapId}
                  className="jvectormap-region">
                </path>
              ))}
            </g>
        </svg>
    </div>
  );
}

export class Map extends React.Component {
  constructor(props){
    super(props);
    this.state = {region: null, regions: [], recpCt: 0};
    this.handleMouseOver = (region) => {
      this.setState({region})
    };
    this.handleMouseLeave = () => this.setState({region: null});
    this.handleClick = (region) => {
      this.props.stateTo({stateTo: "locations", region});
    }
  }
  componentDidMount(){
    Api.loadRegionsDirectly().then(resp => {
      const rp = JSON.parse(resp.body);
      const recpCt = rp.reduce(
        (rd, rg) => rd += parseInt(rg.count_reception), // eslint-disable-line
        0
      );
      this.setState({
        regions: rp,
        recpCt: recpCt
      })
    });
  }
  render() {
    const { data, currentState, openItem } = this.props; // eslint-disable-line
    const currentRegion = currentState.params.region;
    const { region, regions, recpCt } = this.state;
    return (
      <span>
        <div className="map-wrap clearfix col-md-6">
          <MapDisplay handleMouseLeave={this.handleMouseLeave}
                      handleMouseOver={this.handleMouseOver}
                      handleClick={this.handleClick}
                      hoverRegion={region}
                      selectedRegion={currentRegion}
                      regions={regions}
                       />
          <div className="region-info">
              <h3>{(region && region.title) ||
                   (currentRegion && currentRegion.title) ||
                   <FormattedMessage {...messages.uzbekistan} />
              }</h3>
              <p>
                <FormattedMessage {...messages.recpCount} />:{" "}
                {
                  (region && region.count_reception) ||
                  (currentRegion && currentRegion.count_reception) ||
                   recpCt}
              </p>
          </div>
        </div>
        {
          (currentState.id === "locations" &&
          <LocationsList {...this.props} />) ||
          <List {...this.props} />
        }
      </span>
    );
  }
}

export default Map;
