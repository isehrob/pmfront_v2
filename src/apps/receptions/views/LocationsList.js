import React from 'react';
import {FormattedMessage} from 'react-intl';
import { Scrollbars } from 'react-custom-scrollbars';

import {withScrollTo} from 'Lib/scroll-to';
import messages from '../messages';


export const LocationsList = ({data, openItem, closeItem, currentState}) => {
  // this prop is a must in currentState.params
  const { region } = currentState.params;
  const locationStyle = {
    maxHeight: "100px",
    marginTop: '20px'
  };
  return (
    <div className="inspections-info col-md-6">
          <div className="inspections-list clearfix">
              <i className="arrow"></i>
              <Scrollbars style={{height: 500}}>
                {data.map(location => (
                  <div className="item col-md-6"
                       key={location.id} style={locationStyle}>
                      <h5>{location.title}</h5>
                      <p>{location.address && location.address.trim()},{" "}
                        <FormattedMessage {...messages.trsPhone} />: {location.phone}</p>
                      <a className="button"
                         onClick={()=>openItem({id: location.id, region: region})}>
                         <FormattedMessage {...messages.more} />
                      </a>
                  </div>
                ))}

                {(!data || data.length === 0)
                    && <h5><FormattedMessage {...messages.noData} /></h5> }
                <div className="clearfix" style={{marginBottom: 50}} />
              </Scrollbars>
          </div>
    </div>
  );
}

export default withScrollTo('map_app_root', false)(LocationsList);
