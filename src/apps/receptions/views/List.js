import React from 'react';
import {FormattedMessage} from 'react-intl';
import { Scrollbars } from 'react-custom-scrollbars';

import messages from '../messages';


export default ({data, openItem}) => (
  <div className="inspections-info col-md-6">
        <div className="inspections-list clearfix">
            <i className="arrow"></i>
            <Scrollbars style={{height: 500}}>
              {data.map(region => (
                <div className="item col-md-6" key={region.id}>
                  <h5 onClick={() => openItem({region})}
                      style={{cursor:"pointer"}}>
                      {region.title}
                  </h5>
                  <div>
                    <FormattedMessage {...messages.recpCount} />: {' '}
                    {region.count_reception}
                  </div>
                </div> )
              )}
              {(!data || data.length === 0)
                  && <h5><FormattedMessage {...messages.noData} /></h5> }
            </Scrollbars>
        </div>
  </div>
);
