import React from 'react';
import {withGoogleMap, withScriptjs, GoogleMap, Marker} from 'react-google-maps';

import {FormattedMessage} from 'react-intl';

import { withScrollTo } from 'Lib/scroll-to';
import { GOOGLEMAPSAPIKEY } from 'Common/constants';
import messages from '../messages';


export const GoogleMaps = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={parseInt(props.markers[0].zoom, 10)}
    defaultCenter={{
      lat: props.markers[0].position.lat,
      lng: props.markers[0].position.lng
    }}
    onClick={props.onMapClick}
  >
  {
    props.markers.map((marker, index) => {
      console.log("kuku", marker)
      return (
        <Marker
          {...marker}
          onRightClick={() => props.onMarkerRightClick(index)}
        />
      )}
    )
  }
  </GoogleMap>
)));


export const DetailView = ({data, closeItem, currentState}) => {
  const { region } = currentState.params;
  console.log("lat:", parseFloat(data.latitude), "lng:", parseFloat(data.longitude))
  const markers = [{
      position: {
        lat: parseFloat(data.latitude),
        lng: parseFloat(data.longitude),
      },
      zoom: data.zoom,
      key: data.title,
      defaultAnimation: 2,
  }];

  return (
    <div className="direction-overlay">
      <div className="over-top">
          <div className="overlay-left clearfix">
              <div className="close clearfix" onClick={closeItem}>
                <FormattedMessage {...messages.close} />
              </div>
              <div className="direction-info clearfix">
                  <h4>{region.title}</h4>
                  <h4>{data.title}</h4>
              </div>
          </div>
          <div className="overlay-list clearfix">
            <div className="location-data">
              <table className="location-data-table">
                <tbody>
                  <tr><td><FormattedMessage {...messages.address} /></td>
                    <td>{data.address}</td></tr>
                  <tr><td><FormattedMessage {...messages.phone} /></td>
                    <td>{data.phone}</td></tr>
                  <tr><td><FormattedMessage {...messages.email} /></td>
                    <td>{data.email}</td></tr>
                  <tr><td><FormattedMessage {...messages.onMap} /></td>
                    <td>
                      <GoogleMaps
                        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${GOOGLEMAPSAPIKEY}`}
                        loadingElement={<div style={{height: "400px"}}>Loading...</div>}
                        containerElement={<div style={{height: "400px"}}/>}
                        mapElement={<div style={{height: "100%"}} />}
                        onMapLoad={null}
                        onMapClick={null}
                        markers={markers}
                        onMarkerRightClick={null}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
  );
}


export default withScrollTo('map_app_root')(DetailView);
