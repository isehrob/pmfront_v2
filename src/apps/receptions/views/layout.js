import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({appState, children}) => (
  <div>
    <div className="step"><i>3</i>
      <h4><FormattedMessage {...messages.step} /></h4></div>
    <h3 className="title"><FormattedMessage {...messages.appTitle} /></h3>
    <div
      className="inspections-addresses-wrap clearfix"
      style={{
        position: "relative"
      }}>
      {children}
    </div>
  </div>
);
