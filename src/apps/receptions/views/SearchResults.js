import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({data, openItem}) => {
  // this prop is a must in currentState.params
  const itemStyle = {
    padding: "20px 40px 40px 40px",
    textAlign: "left",
  };
  return (
    <div className="direction-overlay">
      <div className="row">
        {data.map(location => (
          <div className="item col-md-4" key={location.id} style={itemStyle}>
              <h3>{location.reception_title}</h3>
              {/*<p>{location.address},
                <FormattedMessage {...messages.trsPhone} />: {location.phone}</p>*/}
              <a className="button sp-search-btn"
                 onClick={()=>openItem({
                   id: location.id,
                   region: {
                     id: location.region_id,
                     title: location.region_title
                   }
                 })}>
                 <FormattedMessage {...messages.more} />
              </a>
          </div>
        ))}

        {(!data || data.length === 0)
            && <h5><FormattedMessage {...messages.noData} /></h5> }
      </div>
    </div>
  );
}
