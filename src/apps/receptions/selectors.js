import { createSelector } from 'reselect';

/**
 * Direct selector to the personalWidgets state domain
 */
const selectLocationDataDomain = () => (state) => { return state.get("receptions");}

/**
 * Default selector used by PersonalWidgets
 */

const makeSelectLocationData = () => {

  const something = createSelector(
    selectLocationDataDomain(),
    (substate) => substate.toJS()
  );
  // console.log('somthing in SELECTOR', something);
  return something;
  // const returnedState = selector();
  // console.log('selector returns THIS: ', returnedState)
  // return selector();
}

export default makeSelectLocationData;
export {
  selectLocationDataDomain,
};
