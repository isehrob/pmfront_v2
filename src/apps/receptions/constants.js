/*
 *
 * PersonalWidgets constants
 *
 */

export const LOAD_DATA = 'apps/receptions/LOAD_DATA';
export const CANCEL_LOAD = 'apps/receptions/CANCEL_LOAD';
export const LOAD_DATA_SUCCESS = 'apps/receptions/LOAD_DATA_SUCCESS';
export const LOAD_DATA_FAIL = 'apps/receptions/LOAD_DATA_FAIL';

export const STATE_FORWARD = 'apps/receptions/STATE_TO_FORWARD';
export const STATE_BACKWARD = 'apps/receptions/STATE_BACKWARD';
export const STATE_TO = 'apps/receptions/STATE_TO';

export const LOAD_REGIONS = 'apps/receptions/LOAD_REGIONS';
export const LOAD_LOCATIONS = 'apps/receptions/LOAD_LOCATIONS';
export const LOAD_DETAIL = 'apps/receptions/LOAD_DETAIL';

export const SEARCH_REQUEST = 'apps/receptions/SEARCH_REQUEST';


// appState constants
export const SUBSPHERES = 'apps/receptions/SUBSPHERES';
export const DETAIL = 'apps/receptions/DETAIL';
