
import {
  loadDataList,
} from '../actions';
import {
  LOAD_DATA_LIST,
} from '../constants';

describe('PersonalWidgets actions', () => {
  describe('Default Action', () => {
    it('has a type of DEFAULT_ACTION', () => {
      const expected = {
        type: LOAD_DATA_LIST,
      };
      expect(loadDataList()).toEqual(expected);
    });
  });
});
