
import { fromJS } from 'immutable';
import reducer from '../reducer';

describe('personalWidgetsReducer', () => {
  it('returns the initial state', () => {
    const expected = fromJS({
      list: [],
      success: true,
      loading: false,
      msg: false,
      currentStack: [],
      stateArray: ['subsphere', 'item']
    });
    expect(reducer(undefined, {})).toEqual(expected);
  });
});
