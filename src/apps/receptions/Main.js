import React from 'react';
import {withSpinner} from 'Lib/spinner';
import DataDetail from './views/Detail';
import SearchResults from './views/SearchResults';
import Map from './views/Map';


export class Main extends React.Component {
  shouldComponentUpdate(newProps){
    if(newProps.appState.loading) {
        return false;
    }
    return true;
  }
  renderComponent(){
    const { appState, openItem, closeItem, goTo } = this.props;
    const currentState = appState.stateStack[0];
    // TODO: need to change this line
    if (currentState === undefined) return null;
    switch (currentState.id) {
      case "detail":
        return <DataDetail data={appState.data}
                           closeItem={closeItem}
                           currentState={currentState} />;
      case "search":
        return <SearchResults data={appState.data ? appState.data : []}
                      openItem={openItem}
                      closeItem={()=> goTo({stateTo: "regions"})}
                      currentState={currentState} />;
      default:
        return (
            <Map data={appState.data ? appState.data : []}
                 openItem={openItem}
                 closeItem={closeItem}
                 stateTo={goTo}
                 currentState={currentState} />
            );
    }
  }
  render(){
    return (
      <div>
        {this.renderComponent()}
      </div>
    );
  }
}

export default withSpinner("appState")(Main);
