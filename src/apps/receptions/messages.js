/*
 * PersonalWidgets Messages
 *
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  showAll: {
    id: 'apps.Receptions.showAll',
    defaultMessage: 'Барча йўналишлар',
  },
  uzbekistan: {
    id: 'apps.Receptions.uzbekistan',
    defaultMessage: "Ўзбекистон Республикаси"
  },
  close: {
    id: 'apps.Receptions.close',
    defaultMessage: 'Орқага',
  },
  step: {
    id: "apps.Receptions.step",
    defaultMessage: "Бўлим"
  },
  noData: {
    id: "apps.Receptions.noData",
    defaultMessage: "Маълумот топилмади"
  },
  search: {
    id: "apps.Receptions.searchBtn",
    defaultMessage: "Излаш"
  },
  address: {
  	id: "apps.Receptions.address",
  	defaultMessage: "Манзил"
  },
  email: {
  	id: "apps.Receptions.email",
  	defaultMessage: "Электрон почта"
  },
  phone: {
  	id: "apps.Receptions.phone",
  	defaultMessage: "Телефон"
  },
  website: {
  	id: "apps.Receptions.website",
  	defaultMessage: "Веб-сайт"
  },
  fax: {
  	id: "apps.Receptions.fax",
  	defaultMessage: "Факс"
  },
  appTitle: {
  	id: "apps.Receptions.appTitle",
  	defaultMessage: "Халқ қабулхоналарининг манзиллари"
  },
  recpCount: {
  	id: "apps.Receptions.recpCount",
  	defaultMessage: "Қабулхоналар сони"
  },
  trsPhone: {
  	id: "apps.Receptions.trsPhone",
  	defaultMessage: "ишонч телефони"
  },
  more: {
  	id: "apps.Receptions.more",
  	defaultMessage: "Батафсил"
  },
  onMap: {
  	id: "apps.Receptions.onMap",
  	defaultMessage: "Харитада"
  }
});
