/*
 *
 *  Root app
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import {createStructuredSelector} from 'reselect';
import makeSelectLocationData from './selectors';
import {
  cancelLoad,
  loadRegions,
  loadLocations,
  loadDetail,
  searchRequest,
  goForward,
  goBackward,
  goTo
} from './actions';
import Search from './search.js';
import AppLayout from './views/layout';
import Main from './Main';


const selectApi = (currentStack, dispatch) => {
  const { id, params } = currentStack;
  switch (id) {
    case "locations":
      dispatch(loadLocations(params.region.id));
      break;
    case "detail":
      dispatch(loadDetail(params.id));
      break;
    case "regions":
      dispatch(loadRegions());
      break;
    case "search":
      dispatch(searchRequest(params.searchTerm));
      break;
    default:
      dispatch(loadRegions());
      break;
    }
}

export class Receptions extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { goTo } = this.props;
    goTo({stateTo: "regions"});
  }
  componentWillReceiveProps(newProps){
    const stateBefore = this.props.appState.stateStack[0],
          stateAfter = newProps.appState.stateStack[0];
    const { dispatch } = newProps;
    // QUESTION: what if you hit search button twice?
    // ANSWER: Oh shit! How can I fix that?
    const shouldCall = !stateBefore ||
          (stateBefore.id !== stateAfter.id) ||
          (JSON.stringify(stateBefore.params) !== JSON.stringify(stateAfter.params));
    if(shouldCall){
      selectApi(stateAfter, dispatch)
    } else if(newProps.appState.loading) {
      this.props.cancelLoad();
    }
  }
  render() {
    const currentState = this.props.appState.stateStack[0];
    return (
      <AppLayout>
        <Search
          closeItem={this.props.closeItem}
          currentState={currentState}
          goTo={this.props.goTo} />
        <Main {...this.props} />
      </AppLayout>
    );
  }
}

Receptions.propTypes = {
  appState: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
  goTo: PropTypes.func.isRequired,
  openItem: PropTypes.func.isRequired,
  closeItem: PropTypes.func.isRequired,
  cancelLoad: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  appState: makeSelectLocationData(),
});

const mapDispatchToProps = (dispatch) => ({
  openItem: (params) => dispatch(goForward(params)),
  closeItem: () => dispatch(goBackward()),
  goTo: (params) => dispatch(goTo(params)),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Receptions);
