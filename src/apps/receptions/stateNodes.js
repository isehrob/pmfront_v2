// these nodes represent app's current state and
// connected to each other as a linked list

export default {
  regions: {
    id: "regions",
    next: "locations",
    before: null
  },
  locations: {
    id: "locations",
    next: "detail",
    before: "regions",
    params: {region: null}
  },
  detail: {
    id: "detail",
    next: null,
    before: "locations",
    params: {region: null, id: null}
  },
  search: {
    id: "search",
    next: "detail",
    before: null,
    params: {searchTerm: ""}
  }
};
