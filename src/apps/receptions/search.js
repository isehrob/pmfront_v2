import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from './messages';


export default class Search extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      searchTerm: ""
    }
    this._placeholder = (window.LANG === "uz" && "Излаш учун матнни киритинг") ||
                        (window.LANG === "ru" && "Введите текст для поиска") ||
                        "Izlash uchun matnni kiriting";
    this.validateInput = (val) => {
      return val.trim() !== "" && String(val);
    }
    this.handleChange = (e) => {
      e.preventDefault();
      const val = e.target.value;
      !this.validateInput(val) && this.props.goTo({stateTo: "regions"});
      this.setState({searchTerm: val});
    }
    this.handleClear = (e) => {
      // e.preventDefault();
      this.setState((prevState, props) => {
        props.currentState.id === "search" &&
        props.goTo({stateTo: "regions"});
        return {searchTerm: ""};
      })
    }
    this.handleSubmit = (e) => {
      e.preventDefault();
      const val = this.state.searchTerm;
      this.validateInput(val) && this.props.goTo({
        searchTerm: this.state.searchTerm,
        stateTo: "search"
      });
    }
  }
  render() {
    return (
      <div className="search-box col-md-12 clearfix">
        <form>
          <i></i>
          <div className="input-wrap">

            <input
              type="text"
              value={this.state.searchTerm}
              onChange={this.handleChange}
              placeholder={this._placeholder} />

            {this.state.searchTerm !== "" ?
              <a className="clear-search-results" onClick={this.handleClear}>
                <span className="glyphicon glyphicon-remove"></span>
              </a>
              : null
            }

          </div>
          <button onClick={this.handleSubmit}>
            <FormattedMessage {...messages.search} />
          </button>
        </form>
      </div>
    );
  }
}
