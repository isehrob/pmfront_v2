/*
 *
 *  Root app
 *
 */

import React from 'react';

import AppLayout from './layout';
import { Api } from './api';
import { MainForm } from 'Form';
import getInitialUserData from 'Form/utils';

const formName = 'RequestFormModel';

const formConfig = {
    form: 'main_application_form',
    initialValues: getInitialUserData(formName),
    formName,
    showFormLabel: false
};

export default () => (
  <AppLayout>
    <MainForm
      formConfig={formConfig}
      apiSubmitPromise={Api.submitApplication}
      authorityData={null}
      prepareFormDataCb={(formData) => {formData[formName].is_agree = 1; return formData;}}
      apiResponseOkHandleCb={resp => resp}
      apiResponseErrorHandleCb={resp => resp}
      full={false}
    />
  </AppLayout>
);
