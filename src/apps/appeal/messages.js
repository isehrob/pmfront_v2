/*
 * PersonalWidgets Messages
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  appTitle: {
    id: 'apps.appeals.appTitle',
    defaultMessage: 'Мурожаат йўллаш турлари', // Способы отправления обращений // Murojaat yo‘llash turlari
  },
  goToRecept: {
    id: 'apps.appeals.goToRecept',
    defaultMessage: 'Яшаш жойингиз бўйича Ўзбекистон Республикаси Президенти Халқ қабулхонасига ташриф буюринг', // Посетите Народную приемную Республики Узбекистан по месту жительства
  },
  callCenter: {
    id: 'apps.appeals.callCenter',
    defaultMessage: '<span>1000</span> ёки <span>0-800-210-00-00</span><br>телефон рақамига қўнғироқ қилинг<br>ва ўз масалангизни оғзаки баён қилинг', // Звоните на телефонный номер<br><span>1000</span> или <span>0-800-210-00-00</span><br>и изложите суть Вашего обращения
  },
  sendAppeal: {
    id: 'apps.appeals.sendAppeal',
    defaultMessage: 'Виртуал қабулхонага мурожаат юборинг', // Отправьте обращение в виртуальную приемную
  }
});
