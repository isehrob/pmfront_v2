import React from 'react';
import {FormattedHTMLMessage, FormattedMessage} from 'react-intl';
import messages from './messages';
import commonMessages from 'Common/messages';


export default class Layout extends React.Component {
  state = {open: false};
  toggle = () => this.setState({open: !this.state.open});
  render () {
    return (
      <span>
        <div className="step">
            <i>4</i>
            <h4><FormattedMessage {...commonMessages.step} /></h4>
        </div>
        <h3 className="title"><FormattedMessage {...messages.appTitle} /></h3>
        <div className="send-appeals-wrap clearfix">

            <ul className="appeals-steps clearfix">

                <li className="item-a col-md-4">
                    <div className="item-wrap">
                        <div className="left">
                            <span>А</span>
                            <i></i>
                        </div>
                        <p className="text"><FormattedMessage {...messages.goToRecept} /></p>
                    </div>
                </li>

                <li className="item-b col-md-5">
                    <div className="item-wrap">
                        <div className="left">
                            <span>Б</span>
                            <i></i>
                        </div>
                        <p className="text">
                          <FormattedHTMLMessage {...messages.callCenter} />
                        </p>
                    </div>
                </li>

                <li className={"item-v col-md-3 " + (this.state.open && "pressed")}>
                    <a className="item-wrap" role="button" onClick={this.toggle}>
                        <div className="left">
                            <span>В</span>
                            <i></i>
                        </div>
                        <p className="text"><FormattedMessage {...messages.sendAppeal} /></p>
                    </a>
                </li>

            </ul>
            {this.state.open &&
              <div style={{paddingTop: 30,
                           borderTop: "1px solid #a9a9a9",
                           marginTop: -20}}>
                {this.props.children}
              </div>
            }
        </div>
      </span>
    );
  }
}
