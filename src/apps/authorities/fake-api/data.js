export const categories = [
  {"id":1,"order":500,"class":"ministries","title":"Вазирликлар"},
  {"id":2,"order":500,"class":"organizations","title":"Марказий муассасалар "},
  {"id":3,"order":500,"class":"state-committees","title":"Давлат қўмиталари "},
  {"id":4,"order":500,"class":"agencies","title":"Агентликлар "},
  {"id":5,"order":500,"class":"committees","title":"Қўмиталар"},
  {"id":6,"order":500,"class":"centers","title":"Марказлар"},
  {"id":14,"order":500,"class":"companies","title":"Бошқа ташкилотлар"},
  {"id":13,"order":500,"class":"societies","title":"Бирлашмалар ва акциядорлик жамиятлар"},
  {"id":12,"order":500,"class":"companies","title":"Компаниялар"},
  {"id":11,"order":500,"class":"associations","title":"Уюшма ва концернлар\t\t\t\t"},
  {"id":10,"order":500,"class":"","title":"Жамоат ташкилотлари"},
  {"id":9,"order":500,"class":"foundations","title":"Фондлар"},
  {"id":7,"order":500,"class":"inspections","title":"Инспекциялар"},
  {"id":8,"order":500,"class":"local-administrations","title":"Маҳаллий давлат ҳокимияти органлари"}
];

export const bodies = [
  {"id":1,"title":" Ўзбекистон Республикаси Иқтисодиёт Вазирлиги","order":500},
	{
    "id":3,
  	"title":"Ўзбекистон Республикаси Молия вазирлиги",
  	"address":"100008, Тошкент шаҳри, Мустақиллик майдони, 5",
  	// "phone":"0(371) 239-1252",
  	"website":"www.mf.uz",
  	"email":"info@mf.uz",
  	"fax":"0(371) 244-5643",
  	"category_id":1,
    "cabinetpm_org_id": 1,
  	"category_title":"Вазирликлар",
  	"category_class":"ministries",
    "functions": "",
    "tasks": "<ul><li>Bibibib</li></ul>",
    guid: "9feb4506-b7d4-ce82-29cc-2efe0b17303d",
  	// "heads":[
  	// 	{
  	// 		"head_title":"Хўжаев Ботир Асадиллаевич",
  	// 		"head_order":10,
  	// 		"head_position":"Вазир",
  	// 		"head_phone":"0(371) 233-1225",
  	// 		"reception_days":"Жума",
  	// 		"reception_time":"16:00 - 18:00",
  	// 		"email":"Priyomnayamf@mf.uz"
  	// 	}
  	// ]
  },
	{"id":4,"title":"Ўзбекистон Республикаси Бандлик ва меҳнат муносабатлар вазирлиги","order":500},
	{"id":5,"title":" Ўзбекистон Республикаси Қишлоқ ва сув хўжалиги вазирлиги","order":500},
	{"id":6,"title":"Ўзбекистон Республикаси Олий ва ўрта махсус таълим вазирлиги","order":500},
	{"id":7,"title":" Ўзбекистон Республикаси Халқ таълими вазирлиги","order":500},
	{"id":8,"title":"Ўзбекистон Республикаси Соғлиқни сақлаш вазирлиги","order":500},
	{"id":9,"title":" Ўзбекистон Республикаси Маданият вазирлиги","order":500},
	{"id":10,"title":" Ўзбекистон Республикаси Мудофаа вазирлиги","order":500},
	{"id":11,"title":"Ўзбекистон Республикаси Ички ишлар вазирлиги","order":500},
	{"id":12,"title":"Ўзбекистон Республикаси Фавқулодда вазиятлар вазирлиги","order":500},
	{"id":13,"title":" Уй-жой коммунал хўжалиги вазирлиги","order":500},
	{"id":14,"title":"Ўзбекистон Республикаси Ташқи ишлар вазирлиги","order":500},
	{"id":15,"title":"Ўзбекистон Республикаси Адлия вазирлиги","order":500},
	{"id":16,"title":"Ўзбекистон Республикаси Ташқи савдо вазирлиги","order":500},
	{"id":17,"title":" Ўзбекистон Республикаси Ахборот технологиялари ва коммуникацияларини ривожлантириш вазирлиги","order":500}
]

export const details = {
	"id":3,
	"title":"Ўзбекистон Республикаси Молия вазирлиги",
	"address":"100008, Тошкент шаҳри, Мустақиллик майдони, 5",
	"website":"www.mf.uz",
	"email":"info@mf.uz",
	"fax":"0(371) 244-5643",
	"category_id":1,
  "cabinetpm_org_id": 1,
	"category_title":"Вазирликлар",
	"category_class":"ministries",
  "functions": "",
  "tasks": "<ul><li>Bibibib</li></ul>",
  guid: "9feb4506-b7d4-ce82-29cc-2efe0b17303d",
}

export const searchResults = [
	{"id":3,
	"authority_title":"Андижон вилояти ҳокимлиги",
	"category_id":1,
	"category_title":"Маҳаллий давлат ҳокимияти органлари",
	"category_class":"local-administrations"
	},
	{"id":89,"authority_title":"Бухоро вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":90,"authority_title":"Жиззах вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":91,"authority_title":"Қашқадарё вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":93,"authority_title":"Навоий вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":95,"authority_title":"Наманган вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":96,"authority_title":"Самарқанд вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":97,"authority_title":"Сурхондарё вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":98,"authority_title":"Сирдарё вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":99,"authority_title":"Тошкент вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":100,"authority_title":"Фарғона вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":101,"authority_title":"Хоразм вилояти ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"},
	{"id":102,"authority_title":"Тошкент шаҳар ҳокимлиги","category_id":8,"category_title":"Маҳаллий давлат ҳокимияти органлари","category_class":"local-administrations"}
];

export const fileUploader = `
<div id="user_file_org" class="btn btn-primary waves-effect waves-light"> <i class="icon-file icon-white"></i> Файлни юкланг</div>
        <div class="file-path-wrapper">
            <input readonly="" id="filename" class="file-path" type="text" placeholder="ҳажми  5мб гача">
        </div>
        <div class="clearfix"></div>
        <div class="dropZone fileUploaderFilesListBox">
            <ul id="user_file_orgFilesList" class="fileUploaderFilesList">

            </ul>
            <div class="clearfix"></div>

             <div class="dropArea">
                <div class="dropZoneTextArea">
                    <p>Перетащите файлы сюда</p>
                </div>
            </div>
        </div>
`;
