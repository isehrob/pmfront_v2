/**
 * Test  sagas
 */

/* eslint-disable redux-saga/yield-effects */
import { take, call, put, select } from 'redux-saga/effects';
import { loadCategoriesSaga } from '../sagas';
import getApi from '../api';

const Api = getApi();

// const generator = defaultSaga();

describe('gvBodiesApp Sagas', () => {
  describe('loadCategoriesSaga', () => {
    it('calls loadCategories api function', () => {
      const gen = loadCategoriesSaga();
      expect(gen.next().value).toEqual(call(Api.loadCategories));
    });
  });
});
