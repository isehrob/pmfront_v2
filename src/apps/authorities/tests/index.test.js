import React from 'react';
import renderer from 'react-test-renderer';
// import { shallow } from 'enzyme';
import {IntlProvider} from 'react-intl';

import { GvBodiesApp } from '../index';
import UselessButtons from '../views/UselessButtons';

const createComponentWithIntl = (
  children,
  props={locale: 'en'}
) => renderer.create(
  <IntlProvider {...props}>
    {children}
  </IntlProvider>
);

describe('<GvBodiesApp />', () => {
  it('<UselessButtons /> renders correctly', () => {
    const component = createComponentWithIntl(<UselessButtons />);
    let tree = component.toJSON();
    expect(component).toMatchSnapshot();
  });
});
