
import {
  loadData,
  loadDataSuccess,
  loadDataFail,
  loadCategories,
  loadBodies,
  loadDetail,
  cancelLoad,
  goForward,
  goBackward,
  goTo,
  searchRequest
} from '../actions';
import {
  LOAD_DATA,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_FAIL,
  LOAD_CATEGORIES,
  LOAD_BODIES,
  LOAD_DETAIL,
  CANCEL_LOAD,
  STATE_FORWARD,
  STATE_BACKWARD,
  STATE_TO,
  SEARCH_REQUEST
} from '../constants';

describe('gvBodiesApp actions', () => {

  it('has a type of LOAD_DATA', () => {
    const expected = {
      type: LOAD_DATA,
    };
    expect(loadData()).toEqual(expected);
  });

  it('has a type of LOAD_DATA_SUCCESS', () => {
    const expected = {
      type: LOAD_DATA_SUCCESS,
      data: []
    };
    expect(loadDataSuccess([])).toEqual(expected);
  });

  it('has a type of LOAD_DATA_FAIL', () => {
    const expected = {
      type: LOAD_DATA_FAIL,
      msg: "something"
    };
    expect(loadDataFail({message: "something"})).toEqual(expected);
  });

  it('has a type of STATE_TO', () => {
    const expected = {
      type: STATE_TO,
      params: {dd: 2},
      stateTo: "categories"
    };
    expect(goTo({stateTo: "categories", dd: 2})).toEqual(expected);
  });

  it('has a type of LOAD_CATEGORIES', () => {
    const expected = {
      type: LOAD_CATEGORIES
    };
    expect(loadCategories()).toEqual(expected);
  });

  it('has a type of LOAD_BODIES', () => {
    const expected = {
      type: LOAD_BODIES,
      catId: 1
    };
    expect(loadBodies(1)).toEqual(expected);
  });

  it('has a type of LOAD_DETAIL', () => {
    const expected = {
      type: LOAD_DETAIL,
      id: 1
    };
    expect(loadDetail(1)).toEqual(expected);
  });

  it('has a type of CANCEL_LOAD', () => {
    const expected = {
      type: CANCEL_LOAD,
    };
    expect(cancelLoad([])).toEqual(expected);
  });

  it('has a type of STATE_FORWARD', () => {
    const expected = {
      type: STATE_FORWARD,
      params: {}
    };
    expect(goForward({})).toEqual(expected);
  });

  it('has a type of STATE_BACKWARD', () => {
    const expected = {
      type: STATE_BACKWARD
    };
    expect(goBackward()).toEqual(expected);
  });

  it('has a type of SEARCH_REQUEST', () => {
    const expected = {
      type: SEARCH_REQUEST,
      searchTerm: "Qisqichbaqa"
    };
    expect(searchRequest("Qisqichbaqa")).toEqual(expected);
  });

});
