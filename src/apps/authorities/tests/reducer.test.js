
import { fromJS } from 'immutable';
import reducer from '../reducer';

import stateNodes from '../stateNodes';

import * as csts from '../constants';

// doing this in order to fix the reference nature of
// js variable assignment and create exact copy of the object
// taken from: https://stackoverflow.com/questions/728360/how-do-i-correctly-clone-a-javascript-object
const makeCloneOf = (obj) => JSON.parse(JSON.stringify(obj));

describe('gvBodiesApp reducers', () => {

  const stateShape = {
    data: null,
    success: true,
    loading: false,
    msg: false,
    stateStack: [],
  };

  it('returns the initial state', () => {
    const expected = fromJS(stateShape);
    expect(reducer(undefined, {})).toEqual(expected);
  });

  it('sets state to loading correctly', () => {
    const currentState = makeCloneOf(stateShape);
    currentState.loading = true;
    const expected = fromJS(currentState);
    expect(reducer(undefined, {type: csts.LOAD_DATA})).toEqual(expected);
  });

  it('sets state to fail correctly', () => {
    const currentState = makeCloneOf(stateShape);
    currentState.success = false;
    currentState.msg = "Somethin bad happened";
    const expected = fromJS(currentState);
    expect(
      reducer(
        undefined,
        {
          type: csts.LOAD_DATA_FAIL,
          msg: "Somethin bad happened"
        })
    ).toEqual(expected);
  });

  it('sets state to success correctly', () => {
    const currentState = makeCloneOf(stateShape);
    currentState.data = ["kuku"];
    const expected = fromJS(currentState);
    expect(
      reducer(
        undefined,
        {
          type: csts.LOAD_DATA_SUCCESS,
          data: ["kuku"]
        })
    ).toEqual(expected);
  });

  it('sets state to specified graph node correctly', () => {
    const currentState = makeCloneOf(stateShape);
    currentState.stateStack = [stateNodes.categories];
    currentState.loading = true;
    const expected = fromJS(currentState);
    expect(
      reducer(
        undefined,
        {
          type: csts.STATE_TO,
          stateTo: "categories",
        })
    ).toEqual(expected);
  });

  it('forwards state to the next graph node correctly', () => {
    const currentState = makeCloneOf(stateShape);
    const nextNode = makeCloneOf(stateNodes.bodies);
    nextNode.params = {dudu: 1, kuku: 2};
    currentState.stateStack.unshift(stateNodes.categories);
    const initialState = fromJS(currentState);
    currentState.stateStack.unshift(nextNode);
    currentState.loading = true;
    const expected = fromJS(currentState);
    expect(
      reducer(
        initialState,
        {
          type: csts.STATE_FORWARD,
          params: {dudu: 1, kuku: 2},
        })
    ).toEqual(expected);
  });

  it('backwards state to the pervious graph node correctly', () => {
    const currentState = makeCloneOf(stateShape);
    const secondNode = makeCloneOf(stateNodes.bodies);
    const thirdNode = makeCloneOf(stateNodes.detail);
    secondNode.params = {dudu: 1, kuku: 2};
    thirdNode.params = {juju: 2, pupu: 3};
    currentState.stateStack.unshift(stateNodes.categories);
    currentState.stateStack.unshift(secondNode);
    currentState.loading = true;
    const expected = fromJS(currentState);
    currentState.stateStack.unshift(thirdNode);
    const initialState = fromJS(currentState);
    expect(
      reducer(
        initialState,
        {
          type: csts.STATE_BACKWARD
        })
    ).toEqual(expected);
  });

  it('empties stateStack when receives "categories" node', () => {
    const currentState = makeCloneOf(stateShape);
    const secondNode = makeCloneOf(stateNodes.bodies);
    const thirdNode = makeCloneOf(stateNodes.detail);
    secondNode.params = {dudu: 1, kuku: 2};
    thirdNode.params = {juju: 2, pupu: 3};
    currentState.stateStack.unshift(stateNodes.categories);
    currentState.stateStack.unshift(secondNode);
    currentState.loading = true;
    currentState.stateStack.unshift(thirdNode);
    const initialState = fromJS(currentState);
    currentState.stateStack = fromJS([stateNodes.categories]);
    const expected = fromJS(currentState);
    expect(
      reducer(
        initialState,
        {
          type: csts.STATE_TO,
          stateTo: "categories"
        })
    ).toEqual(expected);
  });

});
