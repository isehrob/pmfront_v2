/*
 *
 * App reducers
 *
 */

import {fromJS} from 'immutable';

import {
  LOAD_DATA,
  CANCEL_LOAD,
  UPDATE_SEARCH_TERM,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_BODIES_SUCCESS,
  LOAD_SEARCH_RESULTS_SUCCESS,
  // LOAD_DETAIL_SUCCESS,
  LOAD_DATA_FAIL,
} from './constants';

const initialState = fromJS({
  categories: undefined,
  bodies: undefined,
  searchResults: undefined,
  searchTerm: "",
  success: true,
  loading: false,
  msg: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_DATA:
      return state.set('loading', true);
    case LOAD_CATEGORIES_SUCCESS:
      return state.set('categories', fromJS(action.data))
                  .set('loading', false)
                  .set('success', true);
    case LOAD_BODIES_SUCCESS:
      return state.set('bodies', fromJS(action.data))
                  .set('loading', false)
                  .set('success', true);
    case LOAD_SEARCH_RESULTS_SUCCESS:
      return state.set('searchResults', fromJS(action.data))
                  .set('loading', false)
                  .set('success', true);
    case UPDATE_SEARCH_TERM:
      return state.set("searchTerm", action.data);
    case LOAD_DATA_FAIL:
      return state.set('loading', false)
                  .set('msg', action.msg)
                  .set('success', false);
    case CANCEL_LOAD:
      return state.set("loading", false);
    default:
      return state;
  }
}
