/*
 * PersonalWidgets Messages
 *
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  showAll: {
    id: 'apps.Authorities.showAll',
    defaultMessage: 'Барча йўналишлар',
  },
  close: {
    id: 'apps.Authorities.close',
    defaultMessage: 'Орқага',
  },
  step: {
    id: "apps.Authorities.step",
    defaultMessage: "Бўлим"
  },
  noData: {
    id: "apps.Authorities.noData",
    defaultMessage: "Маълумот топилмади"
  },
  search: {
    id: "apps.Authorities.searchBtn",
    defaultMessage: "Излаш"
  },
  emailShort: {
  	id: "apps.Authorities.emailShort",
  	defaultMessage: "эл. почта"
  },
  phone: {
  	id: "apps.Authorities.phone",
  	defaultMessage: "Телефон"
  },
  phoneShort: {
  	id: "apps.Authorities.phoneShort",
  	defaultMessage: "тел."
  },
  website: {
  	id: "apps.Authorities.website",
  	defaultMessage: "Веб-сайт"
  },
  websiteShort: {
  	id: "apps.Authorities.websiteShort",
  	defaultMessage: "веб-сайт"
  },
  fax: {
  	id: "apps.Authorities.fax",
  	defaultMessage: "Факс"
  },
  faxShort: {
  	id: "apps.Authorities.faxShort",
  	defaultMessage: "факс"
  },
  formLabel: {
  	id: "apps.Authorities.formLabel",
  	defaultMessage: "Мурожаат юбориш"
  },
  appTitle: {
  	id: "apps.Authorities.appTitle",
  	defaultMessage: "Вазирлик ва идора раҳбарларининг фуқароларни<br/> қабул қилиш жадвали ва уларга мурожаат юбориш"
  },
  firstUsellessBtn: {
  	id: "apps.Authorities.firstUsellessBtn",
  	defaultMessage: "Фуқароларни қабул қилиш жадвали"
  },
  secondUselessBtn: {
  	id: "apps.Authorities.secondUselessBtn",
  	defaultMessage: "Давлат органларига мурожаат юбориш"
  },
  opt: {
    id: "apps.Authorities.opt",
    defaultMessage: "Йўқ"
  },
  formInfo: {
    id: "apps.Authorities.formInfo",
    defaultMessage: "Ташкилот виртуал қабулхонасига мурожаат юбориш"
  },
  fio: {
    id: "apps.Authorities.fio",
    defaultMessage: "Ф.И.О."
  },
  position: {
    id: "apps.Authorities.position",
    defaultMessage: "Лавозим"
  },
  receptDays: {
    id: "apps.Authorities.receptDays",
    defaultMessage: "Қабул қилиш графиги"
  },
  orgAdress: {
    id: "apps.Authorities.orgAdress",
    defaultMessage: "Ташкилот почта манзили"
  },
  phone2: {
    id: "apps.Authorities.phone2",
    defaultMessage: "Ташкилот рақами"
  },
  email2: {
    id: "apps.Authorities.email2",
    defaultMessage: "Электрон почта манзили"
  },
  days: {
    id: "apps.Authorities.days",
    defaultMessage: "Ҳафта кунлари"
  },
  time: {
    id: "apps.Authorities.time",
    defaultMessage: "Вақти"
  },
  authFirstTab: {
    id: "apps.Authorities.authFirstTab",
    defaultMessage: "Фуқароларни қабул қилиш<br>жадвали ва манзил"
  },
  authSecondTab: {
    id: "apps.Authorities.authSecondTab",
    defaultMessage: "Функция ва вазифалар"
  },
  authThirdTab: {
    id: "apps.Authorities.authThirdTab",
    defaultMessage: "Мурожаат юбориш"
  },
  functions: {
    id: "apps.Authorities.functions",
    defaultMessage: "Функциялар"
  },
  tasks: {
    id: "apps.Authorities.tasks",
    defaultMessage: "Вазифалар"
  },
  error: {
    id: "apps.Authorities.error",
    defaultMessage: "Хато!"
  },
  printButton: {
    id: "apps.Authorities.printButton",
    defaultMessage: "Чоп этиш"
  },
  unknownError:{
    id: "apps.Authorities.unknownError",
    defaultMessage: "Хатолик юз берди!"
  }
});
