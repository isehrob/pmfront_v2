/*
 *
 * PersonalWidgets constants
 *
 */

export const LOAD_DATA = 'apps/authorities/LOAD_DATA';
export const CANCEL_LOAD = 'apps/authorities/CANCEL_LOAD';
export const LOAD_DATA_SUCCESS = 'apps/authorities/LOAD_DATA_SUCCESS';
export const LOAD_DATA_FAIL = 'apps/authorities/LOAD_DATA_FAIL';

export const STATE_FORWARD = 'apps/authorities/STATE_TO_FORWARD';
export const STATE_BACKWARD = 'apps/authorities/STATE_BACKWARD';
export const STATE_TO = 'apps/authorities/STATE_TO';

export const LOAD_CATEGORIES = 'apps/authorities/LOAD_CATEGORIES';
export const LOAD_BODIES = 'apps/authorities/LOAD_BODIES';
export const LOAD_DETAIL = 'apps/authorities/LOAD_DETAIL';
export const LOAD_SEARCH_RESULTS = 'apps/authorities/LOAD_SEARCH_RESULTS';

export const LOAD_CATEGORIES_SUCCESS = 'apps/authorities/LOAD_CATEGORIES_SUCCESS';
export const LOAD_BODIES_SUCCESS = 'apps/authorities/LOAD_BODIES_SUCCESS';
export const LOAD_DETAIL_SUCCESS = 'apps/authorities/LOAD_DETAIL_SUCCESS';
export const LOAD_SEARCH_RESULTS_SUCCESS = 'apps/authorities/LOAD_SEARCH_RESULTS_SUCCESS';

export const UPDATE_SEARCH_TERM = 'apps/authorities/UPDATE_SEARCH_TERM';
export const SEARCH_REQUEST = 'apps/authorities/SEARCH_REQUEST';

// appState constants
export const SUBSPHERES = 'apps/authorities/SUBSPHERES';
export const DETAIL = 'apps/authorities/DETAIL';
