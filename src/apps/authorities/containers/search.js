import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {push} from 'react-router-redux';

import makeSelectData from '../selectors';

import {
  updateSearchTerm,
} from '../actions';

import SearchView from '../views/search';


export class Search extends React.Component {

  render() {
    const {appState, goSearch, goHome, updateSearchTerm} = this.props;
    return (<SearchView
              searchTerm={appState.searchTerm}
              goSearch={goSearch}
              goHome={goHome}
              updateSearchTerm={updateSearchTerm}
            />)
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  goHome: () => dispatch(push(`/`)),
  goSearch: (term) => dispatch(push(`/authorities/search/${term}`)),
  updateSearchTerm: (term) => dispatch(updateSearchTerm(term)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Search);
