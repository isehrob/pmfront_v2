import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {createStructuredSelector} from 'reselect';
import makeSelectData from '../selectors';

import {withScrollTo} from 'Lib/scroll-to';
import {Spinner} from 'Lib/spinner';

import {
  loadSearchResults,
  updateSearchTerm,
  loadData,
  cancelLoad
} from '../actions';

import SearchResultsView from '../views/search-results';


export class SearchResults extends React.Component {

  componentDidMount(){
    this.props.loadData()
    const term = this.props.appState.searchTerm;
    this.props.loadSearchResults(term);
  }
  render() {
    const {searchResults, loading} = this.props.appState;
    if(!searchResults || loading) return <Spinner height={250} />
    return <SearchResultsView
              results={searchResults}
              goTo={this.props.goTo} />
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  goBack: () => dispatch(push('/')),
  goTo: (link) => dispatch(push(link)),
  updateSearchTerm: (term) => dispatch(updateSearchTerm(term)),
  loadSearchResults: (term) => dispatch(loadSearchResults(term)),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

SearchResults = withScrollTo('rc_app_root')(SearchResults);

export default connect(mapStateToProps, mapDispatchToProps)(SearchResults);
