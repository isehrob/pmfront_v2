import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {push} from 'react-router-redux';
import makeSelectData from '../selectors';

import {
  loadCategories,
  loadData,
  cancelLoad
} from '../actions';

// import {withScrollTo} from '../../../lib/ScrollTo';
import {Spinner} from 'Lib/spinner';
import ListView from '../views/list';


export class List extends React.Component {

  componentDidMount(){
    const { categories } = this.props.appState;
    if(!categories){
      this.props.loadData()
      this.props.loadCategories()
    }
  }
  render() {
    const {categories, loading} = this.props.appState;
    if(!categories || loading) return <Spinner height={295} width="99%" />
    return <ListView categories={categories} loadBodies={this.props.loadBodies}/>
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch) => ({
  loadBodies: (link) => dispatch(push(link)),
  loadCategories: () => dispatch(loadCategories()),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

// List = withScrollTo('rc_app_root')(List);

export default connect(mapStateToProps, mapDispatchToProps)(List);
