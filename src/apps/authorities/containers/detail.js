import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {push} from 'react-router-redux';

import makeSelectData, {selectBody} from '../selectors';
import {withScrollTo} from 'Lib/scroll-to';
import {Spinner} from 'Lib/spinner';
import {
  loadBodies,
  loadData,
  cancelLoad
} from '../actions';
// import NotFoundView from '../../../common/components/not-found';
import DetailView from '../views/detail';


export class Detail extends React.Component {
  componentDidMount(){
    this.props.loadData();
    this.props.loadBodies();
  }
  shouldComponentUpdate(nextProps, nextState){
    const { newSearchTerm } = nextProps.appState;
    const { oldSearchTerm } = this.props.appState;
    return newSearchTerm === oldSearchTerm;
  }
  getBody = () => {
    const id = this.props.match.params.body_id;
    const {bodies} = this.props.appState;
    const body = selectBody(bodies, id);
    return body;
  }
  render() {
    const {loading, bodies, searchTerm} = this.props.appState;
    const {goBack, goTo, match} = this.props;
    console.log(!bodies, !this.getBody(), loading)
    if(!bodies || !this.getBody() || loading)
      return <Spinner height={295} width={'99%'} />
    // if(!this.getBody()) return <NotFoundView goBack={goBack} />;
    return <DetailView
              body={this.getBody()}
              searchTerm={searchTerm}
              goBack={goBack}
              goTo={goTo}
              matchParams={match.params} />
  }
}

Detail = withScrollTo('rc_app_root')(Detail);

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  goBack: (searchTerm) => {
    if(searchTerm){
      return dispatch(push(`/authorities/search/${searchTerm}`));
    }
    return dispatch(push(`/authorities/${props.match.params.category_id}`));
  },
  goTo: (link) => dispatch(push(link)),
  loadBodies: () =>dispatch(loadBodies(props.match.params.category_id)),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
