import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {createStructuredSelector} from 'reselect';
import makeSelectData, {selectCategory} from '../selectors';
import {withScrollTo} from 'Lib/scroll-to';
import {Spinner} from 'Lib/spinner';
import {
  loadCategories,
  loadBodies,
  loadData,
  cancelLoad
} from '../actions';

import BodyListView from '../views/body-list';


export class BodyList extends React.Component {

  componentDidMount(){
    const { categories } = this.props.appState;
    this.props.loadData()
    !categories && this.props.loadCategories();
    this.props.loadBodies()
  }
  render() {
    const {bodies, categories, loading} = this.props.appState;
    const {category_id} = this.props.match.params;
    if(!bodies || !categories || loading) return <Spinner height={295} width="99%" />
    return <BodyListView
              bodies={bodies}
              category={selectCategory(categories, category_id)}
              goBack={this.props.goBack} />
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  goBack: () => dispatch(push('/')),
  loadCategories: () => dispatch(loadCategories()),
  loadBodies: () => dispatch(loadBodies(props.match.params.category_id)),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

BodyList = withScrollTo('rc_app_root')(BodyList);

export default connect(mapStateToProps, mapDispatchToProps)(BodyList);
