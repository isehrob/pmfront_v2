import { /*take,*/ call, put, /*cancel,*/ all, takeLatest } from 'redux-saga/effects';
// import { LOCATION_CHANGE } from 'react-router-redux';
import getApi from './api';
import {
  loadDataSuccess,
  loadCategoriesSuccess,
  loadBodiesSuccess,
  loadDetailSuccess,
  loadSearchResultsSuccess,
  loadDataFail,
} from './actions';
import {
  LOAD_CATEGORIES,
  LOAD_BODIES,
  LOAD_DETAIL,
  LOAD_SEARCH_RESULTS,
  SEARCH_REQUEST
} from './constants';

const Api = getApi();

// Individual exports for testing
export function* loadCategoriesSaga() {
  try {
    const resp = yield call(Api.loadCategories);
    yield put(loadCategoriesSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadBodiesSaga(action){
  try {
    const resp = yield call(Api.loadBodies, action.catId);
    yield put(loadBodiesSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadDetailSaga(action){
  try {
    const resp = yield call(Api.loadDetail, action.id);
    yield put(loadDetailSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* searchRequestSaga(action){
  try{
    const resp = yield call(Api.search, action.searchTerm);
    yield put(loadDataSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}
export function* loadSearchResultsSaga(action){
  try{
    const resp = yield call(Api.search, action.searchTerm);
    yield put(loadSearchResultsSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}
export function* watchLoadSearchResultsSaga(){
  yield takeLatest(LOAD_SEARCH_RESULTS, loadSearchResultsSaga);
}

export function* watchLoadCategories() {
  yield takeLatest(LOAD_CATEGORIES, loadCategoriesSaga);
}

// load sphere item saga
export function* watchLoadBodies(){
  yield takeLatest(LOAD_BODIES, loadBodiesSaga);
}

// load sphere item saga
export function* watchLoadDetail(){
  yield takeLatest(LOAD_DETAIL, loadDetailSaga);
}

export function* watchSearchRequestSaga(){
  yield takeLatest(SEARCH_REQUEST, searchRequestSaga);
}

// All sagas to be loaded
export default function* qaRootSaga() {
  yield all([
    watchLoadCategories(),
    watchLoadBodies(),
    watchLoadDetail(),
    watchSearchRequestSaga(),
    watchLoadSearchResultsSaga()
  ]);
}
