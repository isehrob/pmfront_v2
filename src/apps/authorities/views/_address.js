import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({data}) => (
  <div className="text-center">
    {data.address}, {" "}
    <FormattedMessage {...messages.phoneShort}/>: {data.phone}, {' '}
    <FormattedMessage {...messages.faxShort} />: {data.fax}, {' '}
    <FormattedMessage {...messages.websiteShort}/>: {data.website}, {' '}
    <FormattedMessage {...messages.emailShort} />: {data.email} {' '}
  </div>
);
