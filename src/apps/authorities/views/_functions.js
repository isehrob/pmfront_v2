import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({data}) => (
  <table className="functions-display table">
    <thead>
      <tr>
        {data.functions && <td className="col-md-6"><FormattedMessage {...messages.functions} /></td> }
        {data.tasks && <td className="col-md-6"><FormattedMessage {...messages.tasks} /></td> }
      </tr>
    </thead>
    <tbody>
      <tr>
        {data.functions && <td><div dangerouslySetInnerHTML={{__html: data.functions}}/></td>}
        {data.tasks && <td><div dangerouslySetInnerHTML={{__html: data.tasks}}/></td>}
      </tr>
    </tbody>
  </table>
);
