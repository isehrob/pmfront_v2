import React from 'react';
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import messages from '../messages';
import { Api } from '../api';
// import Search from '../search';
import { MainForm } from 'Form';
import getInitialUserData from 'Form/utils';

import Days from './_days';
import Functions from './_functions';
import Address from './_address';


const linkToTabMap = {'_info': 1, '_functions': 2, '_apply': 3};
const tabToLinkMap = {1: '_info', 2: '_functions', 3: '_apply'};


export default class Detail extends React.Component {
  constructor(props){
    super(props);
    this.ApplicationForm = this.getApplicationForm()
  }
  handleSelect = (key) => {
    const {goTo, matchParams} = this.props;
    const {category_id, body_id, tab_id} = matchParams;
    // eslint-disable-next-line
    if(tab_id == tabToLinkMap[key]) return;
    return goTo(
      `/authorities/${category_id}/${body_id}/${tabToLinkMap[key]}`
    )
  }
  _isDaysDisabled = () => {
    const { body } = this.props;
    return !(body.heads || body.phone)
  }
  _isFunctionsDisabled = () => {
    const { body } = this.props;
    return !(body.functions || body.tasks)
  }
  getApplicationForm = () => {
    const formName = 'OrgRequestFormModel';
    const formConfig = {
        form: 'authority_application_form',
        initialValues: getInitialUserData(formName),
        formName
    }; // configuration for `reduxForm`

    return <MainForm
            formConfig={formConfig}
            apiSubmitPromise={Api.submitApplication}
            authorityData={this.props.body}
            prepareFormDataCb={this.prepareFormData}
            apiResponseOkHandleCb={resp => resp}
            apiResponseErrorHandleCb={resp => resp}
            full={false}
          />;
  }
  prepareFormData = (formData) => {
    const { body } = this.props;
    formData.OrgRequestFormModel.authority_id = body.cabinetpm_org_id
    // because server f**k don't understand true
    formData.OrgRequestFormModel.is_agree = 1;
    return formData;
  }
  componentDidMount(){
    const { goTo } = this.props;
    const { category_id, body_id, tab_id } = this.props.matchParams;
    if(this._isDaysDisabled() && tab_id === '_info'){
      goTo(`/authorities/${category_id}/${body_id}/_apply`);
    }
  }
  render(){
    const { body, goBack, matchParams, searchTerm } = this.props;
    const customStyle = {display: "block"};
    const detailDisplayWrapStyle = {paddingBottom: "25px"};
    return (
      <div
        className="direction-overlay authority-detail"
        id="agencies"
        style={customStyle}>

          <div className="auth-detail-controls clearfix">
            <div className="col-md-3 clearfix">
                <div className="close clearfix" onClick={() => goBack(searchTerm)}>
                  <FormattedMessage {...messages.close} />
                </div>
            </div>
            <div className="col-md-9">
            </div>
          </div>

          <h4>{body.title}</h4>

          <div className="over-top clearfix">
            <Tabs
                defaultActiveKey={linkToTabMap[matchParams.tab_id]}
                onSelect={this.handleSelect}
                className="auth-tabs" id="auth-info-tabs">
              <Tab eventKey={1} disabled={this._isDaysDisabled()}
                   title={<FormattedHTMLMessage {...messages.authFirstTab} />}>
                <div className="col-md-12 clearfix"
                     style={detailDisplayWrapStyle}>
                  <div className="authority-card clearfix">
                    <Address data={body} />
                  </div>
                  { !!body && body.heads && <Days data={body} /> }
                </div>
              </Tab>
              <Tab eventKey={2} disabled={this._isFunctionsDisabled()}
                   title={<FormattedMessage {...messages.authSecondTab} />}>
                <div className="col-md-12 clearfix">
                  <h3 style={{
                        textAlign: "center",
                        marginBottom: "50px"
                      }}>
                    <Functions data={body} />
                  </h3>
                </div>
              </Tab>
              <Tab eventKey={3}
                   title={<FormattedMessage {...messages.authThirdTab} />}>
                <div className="col-md-12 clearfix">
                  { this.ApplicationForm }
                </div>
              </Tab>
            </Tabs>
          </div>
      </div>
    );
  }
}
