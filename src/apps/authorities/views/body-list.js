import React from 'react';
import {FormattedMessage} from 'react-intl';
import {Link} from 'react-router-dom';

import messages from '../messages';


export default ({bodies, category, goBack}) => (
  <div className="direction-overlay authority-list">
      <div className="over-top">
          <div className="overlay-left clearfix">
              <div className="close clearfix" onClick={goBack}>
                <FormattedMessage {...messages.close} />
              </div>
              <div className="direction-info clearfix">
                  <i className={category['class']}></i>
                  <h6>{category.title}</h6>
              </div>
          </div>
          <div className="overlay-list clearfix">
              <ul className="authorities clearfix">
                {bodies.map((item, i) => (
                  <li key={i}>
                    <Link to={`/authorities/${category.id}/${item.id}/_info`}>{item.title}</Link>
                  </li>
                ))}
                { !bodies.length === 0 ?
                    <h3>
                      <FormattedMessage {...messages.noData} />
                    </h3>
                    : null }
              </ul>
          </div>
      </div>
      <div className="over-bottom"></div>
  </div>
);
