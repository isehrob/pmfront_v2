import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({categories, loadBodies}) => (
  <ul className="state-organs-types clearfix">
      { categories.map((item, i) => (
        <li className={item['class']}
            key={i}
            onClick={() => loadBodies(`/authorities/${item.id}`)}>
          <a>
            <i></i><span>{item.title}</span>
          </a>
        </li>
      )) }
      { categories.length === 0 ?
          <h3>
            <FormattedMessage {...messages.noData} />
          </h3>
          : null }
  </ul>
);
