import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({results, goTo}) => {
  return (
    <div className="direction-overlay">
      <div className="overlay-list clearfix">
          <ul className="authorities clearfix">
          {results.map(item => (
            <li key={item.id}>
              <a onClick={
                () => goTo(`/authorities/${item.category_id}/${item.id}/_info`)
              }>{item.authority_title}</a>
            </li>
          ))}
          { !results || results.length === 0 ?
              <h3><FormattedMessage {...messages.noData} /></h3>
              : null }
          </ul>
      </div>
    </div>
  );
}
