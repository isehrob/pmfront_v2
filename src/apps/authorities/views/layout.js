import React from 'react';
import {FormattedHTMLMessage, FormattedMessage} from 'react-intl';
import messages from '../messages';

// Height of the section for this step
const layoutHeight = 300;

export default ({children}) => (
  <div>
    <div className="step"><i>2</i><h4>
      <FormattedMessage {...messages.step} />
    </h4></div>
    <h3 className="title">
      <FormattedHTMLMessage {...messages.appTitle} />
    </h3>
    <div className="ministry-and-heads-wrap clearfix"
         style={{minHeight: layoutHeight}}>
        <div className="">{children}</div>
    </div>
  </div>
);
