import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({data}) => (
  <div className="clearfix">
      <table className="days-display table">
          <thead>
            <tr>
              {/* <td rowSpan="2"><FormattedMessage {...messages.fio}/></td> */}
              <td rowSpan="2"><FormattedMessage {...messages.position}/></td>
              <td colSpan="2"><FormattedMessage {...messages.receptDays}/></td>
              <td rowSpan="2"><FormattedMessage {...messages.phone2}/></td>
              {/* <td rowSpan="2"><FormattedMessage {...messages.email2}/></td> */}
            </tr>
            <tr>
              <td><FormattedMessage {...messages.days}/></td>
              <td><FormattedMessage {...messages.time}/></td>
            </tr>
          </thead>
          <tbody>
            {data.heads.map((day, i) => (
              <tr key={i}>
                {/*  <td>{day.head_title}</td> */}
                <td>{day.head_position}</td>
                <td>{day.reception_days}</td>
                <td>{day.reception_time}</td>
                <td>{day.head_phone}</td>
                {/* <td>{day.email}</td> */}
              </tr>
            ))}
          </tbody>
      </table>
  </div>
);
