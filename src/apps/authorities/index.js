import React from 'react';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import {IntlProvider} from 'react-intl';

import {history} from '../../router';
import App from './app';
import { LOCALE } from 'Common/constants';
import services from 'Lib/services';

const messages = services.translationService();

export default ({store}) => (
    <Provider store={store} >
      <IntlProvider locale={LOCALE} messages={messages} >
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
      </IntlProvider>
    </Provider>
);
