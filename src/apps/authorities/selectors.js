import { createSelector } from 'reselect';

/**
 * Direct selector to the personalWidgets state domain
 */
const selectDataDomain = () => (state) => { return state.get("authorities");}
const selectCategory = (categories, id) => {
  // eslint-disable-next-line
  return categories && categories.find(cat => cat.id == id);
}
const selectBody = (bodies, id) => {
  // eslint-disable-next-line
  return bodies && bodies.find(bd => bd.id == id);
}
/**
 * Default selector used by PersonalWidgets
 */

const makeSelectData = () => {

  const something = createSelector(
    selectDataDomain(),
    (substate) => substate.toJS()
  );
  // console.log('somthing in SELECTOR', something);
  return something;
  // const returnedState = selector();
  // console.log('selector returns THIS: ', returnedState)
  // return selector();
}

export default makeSelectData;
export {
  selectDataDomain,
  selectCategory,
  selectBody
};
