/*
 *
 * LocationApp actions
 *
 */

import {
  LOAD_DATA,
  CANCEL_LOAD,
  LOAD_DATA_SUCCESS,
  LOAD_DATA_FAIL,
  LOAD_CATEGORIES,
  LOAD_BODIES,
  LOAD_DETAIL,
  LOAD_SEARCH_RESULTS,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_BODIES_SUCCESS,
  LOAD_DETAIL_SUCCESS,
  LOAD_SEARCH_RESULTS_SUCCESS,
  UPDATE_SEARCH_TERM,
  SEARCH_REQUEST
} from './constants';

export const loadData = () => ({
  type: LOAD_DATA
});

export const cancelLoad = () => ({
  type: CANCEL_LOAD
});

export const loadDataSuccess = (resp) => ({
  type: LOAD_DATA_SUCCESS,
  data: resp
});

export const loadCategoriesSuccess = (resp) => ({
  type: LOAD_CATEGORIES_SUCCESS,
  data: resp
});

export const loadBodiesSuccess = (resp) => ({
  type: LOAD_BODIES_SUCCESS,
  data: resp
});

export const loadDetailSuccess = (resp) => ({
  type: LOAD_DETAIL_SUCCESS,
  data: resp
});

export const loadDataFail = (err) => ({
  type: LOAD_DATA_FAIL,
  msg: err.message
});

export const loadCategories = () => ({
  type: LOAD_CATEGORIES
});

export const loadSearchResults = (searchTerm) => ({
  type: LOAD_SEARCH_RESULTS,
  searchTerm
});

export const loadSearchResultsSuccess = (resp) => ({
  type: LOAD_SEARCH_RESULTS_SUCCESS,
  data: resp
});

export const updateSearchTerm = (searchTerm) => ({
  type: UPDATE_SEARCH_TERM,
  data: searchTerm
});

export const loadBodies = (catId) => ({
  type: LOAD_BODIES,
  catId
});

export const loadDetail = (id) => ({
  type: LOAD_DETAIL,
  id
});

export const searchRequest = (searchTerm) => ({
  type: SEARCH_REQUEST,
  searchTerm
});
