/*
 *
 *  Root app
 *
 */

import React from 'react';
import {Switch, Route} from 'react-router-dom';
import AppLayout from './views/layout';
import Search from './containers/search.js';
import SearchResults from './containers/search-results';
import List from './containers/list';
import BodyList from './containers/body-list';
import BodyDetail from './containers/detail';


export default () => (
  <AppLayout>
      <Search />
      <Switch>
        <Route exact path='/authorities/:category_id' component={BodyList} />
        <Route
            path='/authorities/:category_id/:body_id/:tab_id(_info|_functions|_apply)'
            component={BodyDetail} />
        <Route path='/authorities/search/:search_term' component={SearchResults} />
        <Route component={List} />
      </Switch>
  </AppLayout>
);
