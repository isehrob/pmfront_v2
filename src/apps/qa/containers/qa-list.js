import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {createStructuredSelector} from 'reselect';
import makeSelectData, {selectSphere} from '../selectors';

import {Spinner} from 'Lib/spinner';

import {
  loadQuestions,
  loadSpheres,
  loadData,
  cancelLoad
} from '../actions';

import QaListView from '../views/qa-list';


export class QaList extends React.Component {
  state = {
    expanded: false,
  }
  expand = () => this.setState({expanded: !this.state.expanded})
  componentDidMount(){
    const { spheres } = this.props.appState;
    this.props.loadData()
    !spheres && this.props.loadSpheres();
    this.props.loadQuestions();
  }
  render() {
    const {questions, spheres, loading} = this.props.appState;
    const {sphere_id} = this.props.match.params;
    if(!questions || !spheres || loading) return <Spinner height={400} />
    return <QaListView
              questions={questions}
              sphere={selectSphere(spheres, sphere_id)}
              expanded={this.state.expanded}
              expand={this.expand}
              goBack={this.props.goBack} />
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  goBack: () => dispatch(push('/')),
  loadQuestions: () => dispatch(loadQuestions(props.match.params.sphere_id)),
  loadSpheres: () => dispatch(loadSpheres()),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(QaList);
