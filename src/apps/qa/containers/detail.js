import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {push} from 'react-router-redux';

import makeSelectData, {selectSphere, selectQuestion} from '../selectors';
import {withScrollTo} from 'Lib/scroll-to';
import {Spinner} from 'Lib/spinner';
import {
  loadSpheres,
  loadQuestions,
  loadData,
  cancelLoad
} from '../actions';
import DetailView from '../views/detail';
import { Api } from '../api';


export class Detail extends React.Component {
  componentDidMount(){
    Api.incrementWatched(this.props.match.params.question_id);
    const { spheres } = this.props.appState;
    this.props.loadData()
    !spheres && this.props.loadSpheres()
    this.props.loadQuestions()
  }
  getQuestion = () => {
    const id = this.props.match.params.question_id;
    const questions = this.props.appState.questions;
    const question = selectQuestion(questions, id);
    return question;
  }
  getSphere = () => {
    const id = this.props.match.params.sphere_id;
    const spheres = this.props.appState.spheres;
    const sphere = selectSphere(spheres, id);
    return sphere;
  }
  render() {
    const {loading, questions, searchTerm} = this.props.appState;
    const {goBack} = this.props;
    if(!questions || !this.getQuestion() || loading) return <Spinner />
    return <DetailView
              question={this.getQuestion()}
              sphere={this.getSphere()}
              searchTerm={searchTerm || false}
              goBack={goBack} />
  }
}

Detail = withScrollTo('qa_app_root')(Detail);

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  goBack: (searchTerm) => {
    if(searchTerm){
      return dispatch(push(`/qa/search/${searchTerm}`))
    }
    return dispatch(push(`/qa/spheres/${props.match.params.sphere_id}`))
  },
  loadSpheres: () => dispatch(loadSpheres()),
  loadQuestions: () =>dispatch(loadQuestions(props.match.params.sphere_id)),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
