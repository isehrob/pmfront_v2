import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {push} from 'react-router-redux';
import makeSelectData from '../selectors';

import {
  loadSpheres,
  loadData,
  cancelLoad
} from '../actions';

// import {withScrollTo} from '../../../lib/ScrollTo';
import {Spinner} from 'Lib/spinner';
import ListView from '../views/list';


export class List extends React.Component {

  componentDidMount(){
    const { spheres } = this.props.appState;
    if(!spheres){
      this.props.loadData()
      this.props.loadSpheres()
    }
  }
  render() {
    const {spheres, loading} = this.props.appState;
    if(!spheres || loading) return <Spinner />
    return <ListView data={spheres} loadQuestions={this.props.loadQuestions}/>
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch) => ({
  loadQuestions: (link) => dispatch(push(link)),
  loadSpheres: () => dispatch(loadSpheres()),
  loadData: () => dispatch(loadData()),
  cancelLoad: () => dispatch(cancelLoad()),
  dispatch
});

// List = withScrollTo('qa_app_root')(List);
// List = withSpinner('appState')(List);

export default connect(mapStateToProps, mapDispatchToProps)(List);
