import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({results, goTo}) => {
  // this prop is a must in currentState.params
  const searchResultsStyle = {
    width: "100%",
    paddingBottom: 30
  };
  const itemStyle = {
    width: "40%"
  };
  return (
    <div className="direction-overlay">
      <div className="questions-list clearfix" style={searchResultsStyle}>
        <ul className="questions clearfix">
          {results.map(item => (
            <li key={item.id} style={itemStyle}>
              <a onClick={
                () => goTo(`/qa/spheres/${item.sphere_id}/${item.id}`)
              }>{item.question}</a>
            </li>
          ))}
          { results.length === 0 ?
              <h3><FormattedMessage {...messages.noData} /></h3>
              : null }
        </ul>
      </div>
    </div>
  );
}
