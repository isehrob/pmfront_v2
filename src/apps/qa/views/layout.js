import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default (props) => {
  return (
    <div>
      <div className="step"><i>1</i>
        <h4><FormattedMessage {...messages.step} /></h4></div>
        <h3 className="title"><FormattedMessage {...messages.appTitle} /></h3>
      <div className="main-info-wrap clearfix" style={{
          minHeight: 445
        }}>
        {props.children}
      </div>
    </div>
  );
}
