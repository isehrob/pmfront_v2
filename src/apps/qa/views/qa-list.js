import React from 'react';
import {Link} from 'react-router-dom';

import {FormattedMessage} from 'react-intl';
import messages from '../messages';
import {withScrollTo} from 'Lib/scroll-to';

export default withScrollTo('qa_app_root')(({
  questions, sphere, expanded, expand, goBack
}) => {
  const dataLength = questions.length;
  const middle = dataLength && Math.floor(dataLength / 2) + 1;
  const end = questions.length - 1;
  return (
    <div className="direction-overlay">
      <div className="overlay-left clearfix">
        <div className="close clearfix" onClick={goBack}>
          <FormattedMessage {...messages.close} />
        </div>
        <div className="direction-info clearfix">
            <i className={sphere['class']}></i>
            <h6>{sphere.title}</h6>
            <p>{sphere.description}</p>
        </div>
      </div>
      <div className="questions-list clearfix"
           style={(!expanded && {height: 445, overflow: 'hidden'}) || null}>
        { (dataLength && middle) ?
          [ questions.slice(0, middle),
            questions.slice(middle, end) ].map((subArr, i) => {
            return (
              <div className="col-md-6" key={i}>
                <ul className="questions clearfix">
                  {subArr.map(item => (
                    <li key={item.id}
                        style={{width: "100%", float: "none"}}
                    >
                      <Link to={`/qa/spheres/${sphere.id}/${item.id}`}>
                        {item.question}
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            );
          }) : <h3>
                  <FormattedMessage {...messages.noData} />
               </h3>
        }
      </div>
      <div className="more-btn-wrap">
        <a className="more" onClick={expand}>
          <FormattedMessage {...messages.showAll} />
          <i style={(expanded && {transform: "rotate(180deg)"}) || null}></i>
        </a>
      </div>
    </div>
  );
});
