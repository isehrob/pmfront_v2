import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';

export default ({goBack}) => (
  <div className="close clearfix" onClick={goBack}>
    <FormattedMessage {...messages.close} />
  </div>
);
