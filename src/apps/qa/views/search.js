import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default class Search extends React.Component {

  _placeholder = (window.LANG === "uz" && "Излаш учун матнни киритинг") ||
                      (window.LANG === "ru" && "Введите текст для поиска") ||
                      "Izlash uchun matnni kiriting";
  validateInput = (val) => {
    return val.trim() !== "" && String(val);
  }
  handleChange = (e) => {
    e.preventDefault();
    const val = e.target.value;
    if(!this.validateInput(val)){
      return this.handleClear();
    }
    return this.props.updateSearchTerm(val);
  }
  handleClear = () => {
    this.props.updateSearchTerm("")
    const link = window.location.hash.split('/');
    if(link.indexOf('search') > -1 || link.indexOf('spheres') > -1) {
      this.props.goHome();
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    const val = this.props.searchTerm;
    this.validateInput(val) && this.props.goSearch(val);
  }

  render() {
    const style = {
      width: "100%"
    };
    return (
      <div className="search-box col-md-12 clearfix" style={style}>
        <form>
          <i></i>
          <div className="input-wrap">
            <input
              type="text"
              value={this.props.searchTerm}
              onChange={this.handleChange}
              placeholder={this._placeholder} />

            {this.props.searchTerm !== "" ?
              <a className="clear-search-results" onClick={this.handleClear}>
                <span className="glyphicon glyphicon-remove"></span>
              </a>
              : null
            }

          </div>
          <button onClick={this.handleSubmit}>
            <FormattedMessage {...messages.search} />
          </button>
        </form>
      </div>
    );
  }
}
