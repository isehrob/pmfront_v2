import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';


export default ({
  question, goBack, sphere, searchTerm
}) => {
  return (
    <div className="direction-overlay">
      <div className="overlay-left clearfix">
        <div className="close clearfix" onClick={() => goBack(searchTerm)}>
          <FormattedMessage {...messages.close} />
        </div>
        <div className="direction-info clearfix">
            <i className={sphere['class']}></i>
            <h6>{sphere.title}</h6>
            <p>{sphere.description}</p>
        </div>
      </div>
      <div className="questions-list clearfix">
        <div style={{padding: "10px 20px"}}>
          <h2>{question.question}</h2>
          <br/>
          <div dangerouslySetInnerHTML={{__html: question.answer}} />
          <br/>
          <div className="pull-right views">
            <i className="">
              <FormattedMessage {...messages.watched} />:
            </i> { question.views }
          </div>
        </div>
        <div>
          { /* data.authorities &&
            <h4><FormattedMessage {...messages.qaHint} /></h4> */}
        </div>
      </div>
    </div>
  );
}
