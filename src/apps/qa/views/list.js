import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../messages';

export default ({data, loadQuestions}) => {
  const listItemStyle = {
    width: "33%"
  };
  return (
    <ul className="directions clearfix">
        { data.map(item => (
          <li className={item["class"]}
              style={listItemStyle}
              key={item.id}
              onClick={() => loadQuestions(`/qa/spheres/${item.id}`)}
          >
            <a className="direction-item">
              <i></i><span><h6>{item.title}</h6> {item.description}</span>
            </a>
          </li>
        )) }
        { data.length === 0 ?
            <h3>
              <FormattedMessage {...messages.noData} />
            </h3>
            : null }
    </ul>
  );
}
