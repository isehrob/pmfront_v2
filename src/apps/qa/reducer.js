/*
 *
 * App reducers
 *
 */

import {fromJS} from 'immutable';
import {
  LOAD_DATA,
  CANCEL_LOAD,
  LOAD_QUESTIONS_SUCCESS,
  LOAD_SEARCH_RESULTS_SUCCESS,
  LOAD_SPHERES_SUCCESS,
  LOAD_DETAIL_SUCCESS,
  LOAD_DATA_FAIL,
  UPDATE_SEARCH_TERM
} from './constants';

const initialState = fromJS({
  spheres: undefined,
  questions: undefined,
  searchResults: undefined,
  searchTerm: "",
  success: true,
  loading: false,
  msg: false,
});

export default (state = initialState, action) => {
  switch (action.type) {
    case LOAD_DATA:
      return state.set('loading', true);
    case LOAD_SPHERES_SUCCESS:
      return state.set('spheres', action.data)
                  .set('loading', false)
                  .set('success', true);
    case LOAD_QUESTIONS_SUCCESS:
      return state.set('questions', action.data)
                  .set('loading', false)
                  .set('success', true);
    case LOAD_DETAIL_SUCCESS:
      return state.set('question', action.data)
                  .set('loading', false)
                  .set('success', true);
    case LOAD_SEARCH_RESULTS_SUCCESS:
      return state.set('searchResults', action.data)
                  .set('loading', false)
                  .set('success', true);
    case UPDATE_SEARCH_TERM:
      return state.set('searchTerm', action.data);
    case LOAD_DATA_FAIL:
      return state.set('loading', false)
                  .set('success', false);
    case CANCEL_LOAD:
      return state.set("loading", false);
    default:
      return state;
  }
}
