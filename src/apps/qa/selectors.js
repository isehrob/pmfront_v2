import { createSelector } from 'reselect';

/**
 * Direct selector to the personalWidgets state domain
 */
const selectDataDomain = () => (state) => { return state.get("qa");}

const selectSphere = (spheres, id) => {
  // eslint-disable-next-line
  return spheres && spheres.find(sp => sp.id == id);
}

const selectQuestion = (questions, id) => {
  // eslint-disable-next-line
  return questions && questions.find(qs => qs.id == id);
}
/**
 * Default selector used by PersonalWidgets
 */

const makeSelectData = () => {

  const something = createSelector(
    selectDataDomain(),
    (substate) => substate.toJS()
  );
  return something;
}

export default makeSelectData;
export {
  selectDataDomain,
  selectSphere,
  selectQuestion
};
