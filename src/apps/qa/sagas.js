import { /*take,*/ call, put, /*cancel,*/ all, takeLatest } from 'redux-saga/effects';
// import { LOCATION_CHANGE } from 'react-router-redux';
import getApi from './api';
import {
  loadSpheresSuccess,
  loadQuestionsSuccess,
  loadDetailSuccess,
  loadSearchResultsSuccess,
  loadDataFail,
} from './actions';
import {
  LOAD_SPHERES,
  LOAD_QUESTIONS,
  LOAD_DETAIL,
  LOAD_SEARCH_RESULTS
} from './constants';

const Api = getApi();

// Individual exports for testing
export function* loadSpheresSaga() {
  try {
    const resp = yield call(Api.loadCategories);
    yield put(loadSpheresSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadQuestionsSaga(action){
  try {
    const resp = yield call(Api.loadQuestions, action.catId);
    yield put(loadQuestionsSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadDetailSaga(action){
  try {
    const resp = yield call(Api.loadDetail, action.id);
    yield put(loadDetailSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* loadSearchResultsSaga(action){
  try{
    const resp = yield call(Api.search, action.searchTerm);
    yield put(loadSearchResultsSuccess(resp));
  } catch (err) {
    yield put(loadDataFail(err));
  }
}

export function* watchLoadSpheres() {
  yield takeLatest(LOAD_SPHERES, loadSpheresSaga);
}

// load sphere item saga
export function* watchLoadQuestions(){
  yield takeLatest(LOAD_QUESTIONS, loadQuestionsSaga);
}

// load sphere item saga
export function* watchLoadDetail(){
  yield takeLatest(LOAD_DETAIL, loadDetailSaga);
}

export function* watchLoadSearchResultsSaga(){
  yield takeLatest(LOAD_SEARCH_RESULTS, loadSearchResultsSaga);
}

// All sagas to be loaded
export default function* qaRootSaga() {
  yield all([
    watchLoadSpheres(),
    watchLoadQuestions(),
    watchLoadDetail(),
    watchLoadSearchResultsSaga()
  ]);
}
