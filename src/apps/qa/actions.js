/*
 *
 * LocationApp actions
 *
 */

import {
  LOAD_DATA,
  CANCEL_LOAD,
  LOAD_DATA_SUCCESS,
  LOAD_SPHERES_SUCCESS,
  LOAD_QUESTIONS_SUCCESS,
  LOAD_DETAIL_SUCCESS,
  LOAD_DATA_FAIL,
  LOAD_SPHERES,
  LOAD_QUESTIONS,
  LOAD_DETAIL,
  LOAD_SEARCH_RESULTS,
  LOAD_SEARCH_RESULTS_SUCCESS,
  UPDATE_SEARCH_TERM
} from './constants';

export const loadData = () => ({
  type: LOAD_DATA
});

export const cancelLoad = () => ({
  type: CANCEL_LOAD
});

export const loadDataSuccess = (resp) => ({
  type: LOAD_DATA_SUCCESS,
  data: resp
});

export const loadSpheresSuccess = (resp) => ({
  type: LOAD_SPHERES_SUCCESS,
  data: resp
});

export const loadQuestionsSuccess = (resp) => ({
  type: LOAD_QUESTIONS_SUCCESS,
  data: resp
});

export const loadDetailSuccess = (resp) => ({
  type: LOAD_DETAIL_SUCCESS,
  data: resp
});

export const loadDataFail = (err) => ({
  type: LOAD_DATA_FAIL,
  msg: err.message
});

export const loadSpheres = () => ({
  type: LOAD_SPHERES
});

export const loadQuestions = (catId) => ({
  type: LOAD_QUESTIONS,
  catId
});

export const loadDetail = (id) => ({
  type: LOAD_DETAIL,
  id
});

export const loadSearchResults = (searchTerm) => ({
  type: LOAD_SEARCH_RESULTS,
  searchTerm
});

export const loadSearchResultsSuccess = (resp) => ({
  type: LOAD_SEARCH_RESULTS_SUCCESS,
  data: resp
});

export const updateSearchTerm = (searchTerm) => ({
  type: UPDATE_SEARCH_TERM,
  data: searchTerm
});
