/*
 *
 * PersonalWidgets constants
 *
 */

export const LOAD_DATA = 'apps/qa/LOAD_DATA';
export const CANCEL_LOAD = 'apps/qa/CANCEL_LOAD';
export const LOAD_DATA_SUCCESS = 'apps/qa/LOAD_DATA_SUCCESS';
export const LOAD_DATA_FAIL = 'apps/qa/LOAD_DATA_FAIL';

export const STATE_FORWARD = 'apps/qa/STATE_TO_FORWARD';
export const STATE_BACKWARD = 'apps/qa/STATE_BACKWARD';
export const STATE_TO = 'apps/qa/STATE_TO';

export const LOAD_SPHERES = 'apps/qa/LOAD_SPHERES';
export const LOAD_QUESTIONS = 'apps/qa/LOAD_QUESTIONS';
export const LOAD_DETAIL = 'apps/qa/LOAD_DETAIL';
export const LOAD_SEARCH_RESULTS = 'apps/qa/LOAD_SEARCH_RESULTS';

export const LOAD_SPHERES_SUCCESS = 'apps/qa/LOAD_SPHERES_SUCCESS';
export const LOAD_QUESTIONS_SUCCESS = 'apps/qa/LOAD_QUESTIONS_SUCCESS';
export const LOAD_DETAIL_SUCCESS = 'apps/qa/LOAD_DETAIL_SUCCESS';
export const LOAD_SEARCH_RESULTS_SUCCESS = 'apps/qa/LOAD_SEARCH_RESULTS_SUCCESS';

export const LOAD_SPHERES_FAIL = 'apps/qa/LOAD_SPHERES_FAIL';
export const LOAD_QUESTIONS_FAIL = 'apps/qa/LOAD_QUESTIONS_FAIL';
export const LOAD_DETAIL_FAIL = 'apps/qa/LOAD_DETAIL_FAIL';

export const UPDATE_SEARCH_TERM = 'apps/qa/UPDATE_SEARCH_TERM';
export const SEARCH_REQUEST = 'apps/qa/SEARCH_REQUEST';


// appState constants
export const SUBSPHERES = 'apps/qa/SUBSPHERES';
export const DETAIL = 'apps/qa/DETAIL';
