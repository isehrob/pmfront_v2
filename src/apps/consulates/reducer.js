import {fromJS} from 'immutable';

import * as consts from './constants';


const initialState = fromJS({
  consulates: null,
  loading: false,
  errorMsg: null
});


export default (state = initialState, action) => {
  switch (action.type) {
    case consts.LOAD_CONSULATES:
    case consts.LOAD_CONSULATE:
      return state.set('loading', true);
    case consts.LOAD_CONSULATES_SUCCESS:
      return state.set('consulates', action.payload).set('loading', false);
    case consts.LOAD_CONSULATES_FAIL:
      return state.set('consulates', null)
                  .set('loading', false)
                  .set('errorMsg', action.payload);
    case consts.LOAD_CONSULATE_SUCCESS:
    case consts.LOAD_CONSULATE_FAIL:
      return state.set('loading', false);
    default:
      return state;
  }
}

export const selectConsulate = (state, id) => state.find(item => item.id == id);
