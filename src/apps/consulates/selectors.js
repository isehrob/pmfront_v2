import { createSelector } from 'reselect';

/**
 * Direct selector to the personalWidgets state domain
 */
const selectDataDomain = () => (state) => { return state.get("consulates");}
/**
 * Default selector used by PersonalWidgets
 */

const makeSelectData = () => {

  const something = createSelector(
    selectDataDomain(),
    (substate) => substate.toJS()
  );
  // console.log('somthing in SELECTOR', something);
  return something;
  // const returnedState = selector();
  // console.log('selector returns THIS: ', returnedState)
  // return selector();
}

export default makeSelectData;
