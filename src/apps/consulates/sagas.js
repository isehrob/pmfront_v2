import { /*take,*/ call, put, /*cancel,*/ all, takeLatest } from 'redux-saga/effects';
// import { LOCATION_CHANGE } from 'react-router-redux';
import Api from './api';
import {
  loadConsulatesSuccess,
  loadConsulatesFail
} from './actions';
import {
  LOAD_CONSULATES,
} from './constants';

// Individual exports for testing
export function* loadConsulatesSaga() {
  try {
    const resp = yield call(Api.fetchConsulates);
    yield put(loadConsulatesSuccess(resp));
  } catch (err) {
    yield put(loadConsulatesFail(err));
  }
}

export function* watchLoadConsulatesSaga(){
  yield takeLatest(LOAD_CONSULATES, loadConsulatesSaga);
}

// All sagas to be loaded
export default function* qaRootSaga() {
  yield all([
    watchLoadConsulatesSaga(),
  ]);
}
