
export const LOAD_CONSULATES = 'apps/consulates/LOAD_CONSULATES';
export const LOAD_CONSULATE = 'apps/consulates/LOAD_CONSULATE';
export const LOAD_CONSULATES_SUCCESS = 'apps/consulates/LOAD_CONSULATES_SUCCESS';
export const LOAD_CONSULATE_SUCCESS = 'apps/consulates/LOAD_CONSULATE_SUCCESS';
export const LOAD_CONSULATES_FAIL = 'apps/consulates/LOAD_CONSULATES_FAIL';
export const LOAD_CONSULATE_FAIL = 'apps/consulates/LOAD_CONSULATE_FAIL';
