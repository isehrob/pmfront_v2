/*
 * PersonalWidgets Messages
 *
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  appName: {
    id: "apps.consulates.appName",
    defaultMessage: "Ўзбекистоннинг чет давлатлардаги консуллик муассасалари"
  },
  aLabel: {
    id: "apps.consulates.aLabel",
    defaultMessage: "Ўзбекистоннинг чет давлатлардаги консуллик муассасалари" //Консульские учреждение Узбекистана за рубежом
  },
  bLabel: {
    id: "apps.consulates.bLabel",
    defaultMessage: "Чет давлатлардаги фуқаролар учун мурожаат йўллаш формаси" //Форма отправки обращений для граждан за рубежом
  },
});
