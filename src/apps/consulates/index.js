import React from 'react';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'react-router-redux';
import {IntlProvider} from 'react-intl';

import {history} from '../../router';
import App from './app';
import {LOCALE} from 'Common/constants';
import services from 'Lib/services';

import './style.css';


const messages = services.translationService();

export default class extends React.Component {
  state = {show: false}
  componentDidMount() {
    console.log(services)
    services.geolocationService().then(resp => {
      console.log(resp, typeof reps);
      const result = JSON.parse(resp.body);
      const code = result['country_code'];
      const name = result['country_name'];
      if(code !== 'UZ' && name !== 'Uzbekistan'){
        this.setState({show: true});
      }
    });
  }
  render(){
    const store = this.props.store;
    if(this.state.show){
      return (
        <Provider store={store}>
          <IntlProvider locale={LOCALE} messages={messages}>
            <ConnectedRouter history={history}>
              <App />
            </ConnectedRouter>
          </IntlProvider>
        </Provider>
      );
    } else {
      return null;
    }
  }
}
