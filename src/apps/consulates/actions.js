import * as consts from './constants';


export const loadConsulates = () => ({
  type: consts.LOAD_CONSULATES
});

export const loadConsulatesSuccess = (payload) => ({
  type: consts.LOAD_CONSULATES_SUCCESS,
  payload
});

export const loadConsulatesFail = (payload) => ({
  type: consts.LOAD_CONSULATES_FAIL,
  payload
});

export const loadConsulate = (id) => ({
  type: consts.LOAD_CONSULATE,
  id
});

export const loadConsulateSuccess = (payload) => ({
  type: consts.LOAD_CONSULATE_SUCCESS,
  payload
});

export const loadConsulateFail = (payload) => ({
  type: consts.LOAD_CONSULATE_FAIL,
  payload
});
