import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../../authorities/messages';
import mapMessages from '../../receptions/messages';

import {withGoogleMap, withScriptjs, GoogleMap, Marker} from 'react-google-maps';

import { GOOGLEMAPSAPIKEY } from 'Common/constants';


export const GoogleMaps = withScriptjs(withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    defaultZoom={parseInt(props.markers[0].zoom, 10)}
    defaultCenter={{
      lat: props.markers[0].position.lat,
      lng: props.markers[0].position.lng
    }}
    onClick={props.onMapClick}
  >
  {
    props.markers.map((marker, index) => {
      console.log("kuku", marker)
      return (
        <Marker
          {...marker}
          onRightClick={() => props.onMarkerRightClick(index)}
        />
      )}
    )
  }
  </GoogleMap>
)));


export default ({consulate, goBack}) => {

  console.log("lat:", parseFloat(consulate.latitude), "lng:", parseFloat(consulate.longitude))
  const markers = [{
      position: {
        lat: parseFloat(consulate.latitude),
        lng: parseFloat(consulate.longitude),
      },
      zoom: consulate.zoom || 7,
      key: consulate.title,
      defaultAnimation: 2,
  }];

  return (
    <div className="direction-overlay">
      <div className="over-top">
          <div className="overlay-left clearfix">
              <div className="close clearfix" onClick={goBack}>
                <FormattedMessage {...messages.close} />
              </div>
              <div className="direction-info clearfix">
                  <h4>{consulate.title}</h4>
              </div>
          </div>
          <div className="overlay-list clearfix">
            <div className="location-data">
              <table className="location-data-table">
                <tbody>
                  <tr><td><FormattedMessage {...mapMessages.address} /></td>
                    <td>{consulate.address}</td></tr>
                  <tr><td><FormattedMessage {...messages.phone} /></td>
                    <td>{consulate.phone}</td></tr>
                  <tr><td><FormattedMessage {...mapMessages.email} /></td>
                    <td>{consulate.email}</td></tr>
                  <tr><td><FormattedMessage {...messages.website} /></td>
                    <td>{consulate.website}</td></tr>
                  <tr><td><FormattedMessage {...messages.receptDays} /></td>
                    <td>{consulate.reception_days}</td></tr>
                  <tr><td><FormattedMessage {...mapMessages.onMap} /></td>
                    <td>
                      <GoogleMaps
                        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&key=${GOOGLEMAPSAPIKEY}`}
                        loadingElement={<div style={{height: "400px"}}>Loading...</div>}
                        containerElement={<div style={{height: "400px"}}/>}
                        mapElement={<div style={{height: "100%"}} />}
                        onMapLoad={null}
                        onMapClick={null}
                        markers={markers}
                        onMarkerRightClick={null}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>
  );
}
