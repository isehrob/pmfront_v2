import React from 'react';
import {Link} from 'react-router-dom';
import { /* FormattedHTMLMessage, */ FormattedMessage} from 'react-intl';
import commonMessages from 'Common/messages';
import messages from '../messages';

// Height of the section for this step
const layoutHeight = 300;

export default ({children}) => (
  <div>
    <div className="step"><i>5</i><h4>
      <FormattedMessage {...commonMessages.step} />
    </h4></div>
    <h3 className="title">
      <FormattedMessage {...messages.appName} />
    </h3>
    <div className="consulates-wrap clearfix"
         style={{minHeight: layoutHeight}}>
         <ul className="appeals-steps clearfix">
          <li className="item-a col-md-6">
            <Link className="text" to="/consulates/">
              <div className="item-wrap">
                  <div className="left">
                      <span>A</span>
                      <i></i>
                  </div>
                  <p className="text">
                    <FormattedMessage {...messages.aLabel} />
                  </p>
              </div>
            </Link>
          </li>
          <li className="item-b col-md-6">
            <Link to="/consulates/apply">
              <div className="item-wrap">
                  <div className="left">
                      <span>B</span>
                      <i></i>
                  </div>
                  <p className="text">
                    <FormattedMessage {...messages.bLabel} />
                  </p>
              </div>
            </Link>
          </li>
      </ul>

        <div className="">{children}</div>
    </div>
  </div>
);
