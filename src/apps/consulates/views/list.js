import React from 'react';
import {FormattedMessage} from 'react-intl';
import messages from '../../authorities/messages';


export default ({consulates, loadConsulate, expand, expanded}) => (
  <div style={(!expanded && {height: 345, overflow: 'hidden'}) || null}>
    <ul className="state-organs-types clearfix">
        { consulates.map((item, i) => (
          <li className={item['class']}
              style={{width: '33%'}}
              key={i}
              onClick={() => loadConsulate(item.id)}>
            <a style={{height: 110}}>
              <i></i><span>{item.title}</span>
            </a>
          </li>
        )) }
        { consulates.length === 0 ?
            <h3>
              <FormattedMessage {...messages.noData} />
            </h3>
            : null }
    </ul>
    <div className="more-btn-wrap" style={
        {bottom: 22, padding: '20px 0', width: '98%'}
      }>
      <a className="more" onClick={expand}>
        <FormattedMessage {...messages.showAll} />
        <i style={(expanded && {transform: "rotate(180deg)"}) || null}></i>
      </a>
    </div>
  </div>
);
