import React from 'react';

import {Switch, Route} from 'react-router-dom';

import Layout from './views/layout';
import ApplicationForm from './containers/application-form';
import List from './containers/list';
import Detail from './containers/detail';


export default () => (
  <Layout>
    <Switch>
      <Route exact path="/consulates/" component={List} />
      <Route path="/consulates/apply" component={ApplicationForm} />
      <Route path="/consulates/:id" component={Detail} />
      <Route component={List} />
    </Switch>
  </Layout>
);
