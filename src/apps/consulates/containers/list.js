import React from 'react';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import {createStructuredSelector} from 'reselect';
import makeSelectData from '../selectors';
// import {withScrollTo} from 'Lib/scroll-to';
import {Spinner} from 'Lib/spinner';
import {
  loadConsulates,
} from '../actions';

import ListView from '../views/list';


export class List extends React.Component {
  state = {expanded: false}
  expand = () => this.setState({expanded: !this.state.expanded})
  componentDidMount(){
    const { consulates } = this.props.appState;
    !consulates && this.props.loadConsulates();
  }
  render() {
    const {consulates, loading} = this.props.appState;
    if(!consulates || loading) return <Spinner height={100} width="99%" />
    return <ListView consulates={consulates} {...this.props}
                     expanded={this.state.expanded}
                     expand={this.expand}/>
  }
}

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  loadConsulates: () => dispatch(loadConsulates()),
  loadConsulate: (id) => dispatch(push(`/consulates/${id}`)),
  dispatch
});

// List = withScrollTo('consulates_app_root')(List);

export default connect(mapStateToProps, mapDispatchToProps)(List);
