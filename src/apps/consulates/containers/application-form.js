import React from 'react';

import Api from '../api';
import { AbroadForm } from 'Form';
// import {FormattedMessage} from 'react-intl';
// import messages from 'Common/messages';
// import getInitialUserData from 'Form/utils';

const formName = 'ConsulateRequestFormModel';

const formConfig = {
    form: 'consulates_application_form',
    initialValues: undefined,
    formName,
    showFormLabel: false
};


export default class extends React.Component {
  state = {showNotice: true}
  showNotice = () => this.setState({showNotice: true})
  hideNotice = () => this.setState({showNotice: false})
  componentDidMount(){
    if(this.state.showNotice && window.jQuery){
      window.jQuery('#noticeModal').modal('show');
      window.jQuery('#noticeModal').on('hidden.bs.modal', this.hideNotice);
    };
  }
  render () {
    return (
      <span>
        <AbroadForm
            formConfig={formConfig}
            apiSubmitPromise={Api.submitApplication}
            authorityData={null}
            prepareFormDataCb={(formData) => {formData[formName].is_agree = 1; return formData;}}
            apiResponseOkHandleCb={resp => resp}
            apiResponseErrorHandleCb={resp => resp}
            full={false}
          />
      </span>
    );
  }
}
