import React from 'react';
import {connect} from 'react-redux';
import {createStructuredSelector} from 'reselect';
import {push} from 'react-router-redux';

import makeSelectData from '../selectors';
import {selectConsulate} from '../reducer';
// import {withScrollTo} from 'Lib/scroll-to';
import {Spinner} from 'Lib/spinner';
import {
  loadConsulates,
} from '../actions';
import DetailView from '../views/detail';


export class Detail extends React.Component {
  componentDidMount(){
    const { consulates } = this.props.appState;
    !consulates && this.props.loadConsulates()
  }
  getConsulate = () => {
    const id = this.props.match.params.id;
    const {consulates} = this.props.appState;
    return selectConsulate(consulates, id);
  }
  render() {
    const {loading, consulates} = this.props.appState;
    const {goBack} = this.props;
    if(!consulates || loading) return <Spinner />
    return <DetailView
              consulate={this.getConsulate()}
              goBack={goBack} />
  }
}

// Detail = withScrollTo('qa_app_root')(Detail);

const mapStateToProps = createStructuredSelector({
  appState: makeSelectData(),
});

const mapDispatchToProps = (dispatch, props) => ({
  loadConsulates: () => dispatch(loadConsulates()),
  goBack: () => dispatch(push(`/consulates/`)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
