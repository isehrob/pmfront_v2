import React from 'react';
import {Provider} from 'react-redux';
import {IntlProvider} from 'react-intl';
import {ConnectedRouter} from 'react-router-redux';

import {history} from '../../router';
import App from './app';
import getTranslations, {locale} from '../../lib/get-translations';


const messages = getTranslations();

export default ({store}) => (
  <Provider store={store} >
    <IntlProvider locale={locale} messages={messages} >
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
    </IntlProvider>
  </Provider>
);
