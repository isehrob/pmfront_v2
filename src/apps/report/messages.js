/*
 * PersonalWidgets Messages
 *
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  showAll: {
    id: 'apps.Qa.showAll',
    defaultMessage: 'Барча йўналишлар',
  },
  appTitle: {
    id: 'apps.Qa.appTitle',
    defaultMessage: 'Ўзингизни қизиқтирган саволга жавоб топинг',
  },
  close: {
    id: 'apps.Qa.close',
    defaultMessage: 'Орқага',
  },
  step: {
    id: "apps.Qa.step",
    defaultMessage: "Бўлим"
  },
  noData: {
    id: "apps.Qa.noData",
    defaultMessage: "Маълумот топилмади"
  },
  search: {
    id: "apps.Qa.searchBtn",
    defaultMessage: "Излаш"
  },
  qaHint: {
    id: "apps.Qa.qaHint",
    defaultMessage: "Агар саволингизга тўлиқ жавоб олмаган бўлсангиз қуйидаги органларга мурожаат қилишингиз мумкин:"
  },
  watched: {
    id: "apps.Qa.watched",
    defaultMessage: "Кўрилган"
  },
});
