/*
 *
 *  Root app
 *
 */

import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Search from './containers/search.js';
import SearchResults from './containers/search-results';
import AppLayout from './views/layout';
import List from './containers/list';
import Questions from './containers/qa-list';
import QuestionDetail from './containers/detail';


export default () => (
  <AppLayout>
    <Search />
    <Switch>
      <Route path="/qa/search/:search_term" component={SearchResults}/>
      <Route exact path="/qa/spheres/:sphere_id" component={Questions} />
      <Route exact path="/qa/spheres/:sphere_id/:question_id" component={QuestionDetail} />
      <Route component={List} />
    </Switch>
  </AppLayout>
);
