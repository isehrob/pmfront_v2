import React from 'react';

import services from 'Lib/services';
import QaAppPage from './components/error-views/qa-app';
import RcAppPage from './components/error-views/rc-app';
import MapAppPage from './components/error-views/map-app';


const AppPageMap = {
  'qa_app': <QaAppPage />,
  'rc_app': <RcAppPage />,
  'map_app': <MapAppPage />
}

class ErrorHandler extends React.Component {
  state = {hasError: false}
  componentDidCatch(error, info){
    this.setState({hasError: true});
    services.loggerService(error, info);
  }
  componentDidUpdate(){
    if(this.state.hasError){
      this.tm = setTimeout(this._ask, 3000)
    }
  }
  componentWillUnmount(){
    clearTimeout(this.tm);
  }
  _ask(){
    const resp = window.confirm('Что-то пошло не так. Обновить страницу?');
    resp && window.location.reload();
  }
  render(){
    if(this.state.hasError){
      return AppPageMap[this.props.appLabel];
    }
    return this.props.children;
  }
}

export default ErrorHandler;
