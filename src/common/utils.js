
// utility function to make fake api calls
export const delay = (ms) =>
  new Promise(resolve => setTimeout(resolve, ms));
