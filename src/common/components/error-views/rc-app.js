import React from 'react';

export default () => (
  <div>
    <div className="step">
      <i>2</i>
      <h4><span>Раздел</span></h4>
    </div>
    <h3 className="title"><span>Отправка обращений и приемные дни министерств и ведомств</span></h3>
    <div className="ministry-and-heads-wrap clearfix">
      <div className="">
        <div className="search-box col-md-12 clearfix">
          <form>
            <i></i>
            <div className="input-wrap">
              <input type="text" value="" placeholder="Введите текст для поиска" />
            </div>
            <button><span>Поиск</span></button>
          </form>
        </div>
        <div>
          <div>
            <ul className="state-organs-types clearfix">
              <li className="ministries">
                <a><i></i><span>Министерства</span></a>
              </li>
              <li className="organizations">
                <a><i></i><span>Центральные учреждения</span></a>
              </li>
              <li className="state-committees">
                <a><i></i><span>Государственные комитеты</span></a>
              </li>
              <li className="agencies">
                <a><i></i><span>Агентства</span></a>
              </li>
              <li className="committees">
                <a><i></i><span>Комитеты</span></a>
              </li>
              <li className="centers">
                <a><i></i><span>Центры</span></a>
              </li>
              <li className="societies">
                <a><i></i><span>Объединения и акционерные общества </span></a>
              </li>
              <li className="companies">
                <a><i></i><span>Компании</span></a>
              </li>
              <li className="associations">
                <a><i></i><span>Ассоциации и концерны</span></a>
              </li>
              <li className="foundations">
                <a><i></i><span>Фонды</span></a>
              </li>
              <li className="local-administrations">
                <a><i></i><span>Органы государственной власти на местах</span></a>
              </li>
              <li className="jamorg">
                <a><i></i><span>Общественные организации</span></a>
              </li>
              <li className="others">
                <a><i></i><span>Другие организации</span></a>
              </li>
              <li className="inspections">
                <a><i></i><span>Инспекции</span></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);
