import React from 'react';


export default () => (
  <div>
    <div className="step">
      <i>1</i>
        <h4><span>Раздел</span></h4>
    </div>
    <h3 className="title">
    <span>Получайте ответы на интересующие вас вопросы</span>
    </h3>
    <div className="main-info-wrap clearfix">
      <div className="search-box col-md-12 clearfix" style={{width: '100%'}}>
        <form>
          <i></i>
          <div className="input-wrap">
            <input type="text" value="" placeholder="Введите текст для поиска" />
          </div>
          <button><span>Поиск</span></button>
        </form>
      </div>
      <div>
        <div className="col-md-12" style={{paddingLeft: 0}}>
          <div className="directions-wrap" style={{overflow: 'inherit', marginBottom: 30}}>
            <ul className="directions clearfix">
              <li className="house" style={{width: '33%'}}>
                <a className="direction-item">
                  <i></i><span><h6>Жилищное хозяйство</h6> Купля-продажа, регистрация и порядок государственной регистрации</span>
                </a>
              </li>
              <li className="protection" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Социальная защита</h6> Пенсии, пособия по уходу за ребенком, финансовая помощь</span>
                </a>
              </li>
              <li className="business" style={{width: '33%'}}>
                <a className="direction-item">
                  <i></i><span><h6>Предпринимательская деятельность</h6> Законодательство, лицензирование, защита предпринимательства, импорт, экспорт</span>
                </a>
              </li>
              <li className="bank" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Банковская деятельность</h6> Законодательство, финансовый кредит, депозит</span>
                </a>
              </li>
              <li className="production" style={{width: '33%'}}>
                <a className="direction-item">
                  <i></i><span><h6>Производство, сертификация и ветеринарные услуги</h6> Промышленность, сертификация, скотоводство, домашние животные</span>
                </a>
              </li>
              <li className="taxes" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Налоги и штрафы</h6> Законодательство, налоги и их виды, штрафы, проверки</span>
                </a>
              </li>
              <li className="communication" style={{width: '33%'}}>
                <a className="direction-item">
                  <i></i><span>
                    <h6>Информационные технологии и коммуникации</h6> Телефон, интернет, информационные системы, лицензирование, телекоммуникация, мобильная связь</span>
                </a>
              </li>
              <li className="transport" style={{width: '33%'}}>
                <a className="direction-item">
                  <i></i>
                    <span><h6>Транспортные вопросы</h6> Общественный транспорт, автотранспортные средства, автомобильные дороги, железная дорога, воздушный транспорт, безопасность дорожного движения</span>
                </a>
              </li>
              <li className="others" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Разное</h6> Другие направления социальной жизни</span>
                </a>
              </li>
              <li className="legal" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Правовые вопросы</h6> Законодательство, адвокатура, социальные вопросы, права человека</span>
                </a>
              </li>
              <li className="education" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Образование, наука и культура</h6> Законодательство, виды образования, наука и культура</span>
                </a>
              </li>
              <li className="labor" style={{width: '33%'}}>
                <a className="direction-item"><i></i>
                  <span><h6>Труд и занятость населения</h6> Трудовое законодательство, трудовые отношения и занятость населения</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
);
