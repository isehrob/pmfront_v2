/*
 * PersonalWidgets Messages
 *
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  step: {
    id: "apps.step",
    defaultMessage: "Бўлим"
  },
  close: {
    id: 'apps.close',
    defaultMessage: 'Орқага',
  },
  mainFormTitle: {
    id: 'apps.mainFormTitle',
    defaultMessage: 'Ўзбекистон Республикаси Президенти Виртуал Қабулхонаси',
  },
});
