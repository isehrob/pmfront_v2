import langs from 'Lib/langs';

// eslint-disable-next-line
const timeouts = {
    response: 15000,  // Wait 15 seconds for the server to start sending,
    deadline: 20000, // but allow 20 seconds for the file to finish loading.
};
export const LOCALE = window.LANG ? langs[window.LANG] : "en";
export const APP_LANG = window.LANG ? window.LANG : "uz";
export const HOST = window.location.host;

export const API_BASE_URL = (HOST === "pm.gov.uz" || HOST === "www.pm.gov.uz") ?
  `https://${HOST}/${APP_LANG}` :
  `http://${HOST}/${APP_LANG}`;

  export const NEW_API_BASE_URL = (HOST === "pm.gov.uz" || HOST === "www.pm.gov.uz") ?
    `https://${HOST}/${APP_LANG}/api` :
    `http://${HOST}/${APP_LANG}/api`;

// Ask api key from SEHROB
export const GOOGLEMAPSAPIKEY = 'AIzaSyDuOkHwjYE7G9oeSYbRlveuyGpn5MI4oVY';
