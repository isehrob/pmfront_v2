import React from 'react';
import {reduxForm} from 'redux-form/immutable';

import {FormattedMessage} from 'react-intl';
import messages from '../messages';

import Spinner from 'Lib/spinner';

import SmsForm from '../forms/sms-check';
import MainForm from '../forms/main';
import SubmittedView from '../submitted';

import { Api } from '../api';


export default class extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        loading: false,
        submitted: false,
        hasErrors: false,
        errorMessage: <FormattedMessage {...messages.unknownError} />,
        result: {},
        smsChecking: false,
        smsCheckingDone: false,
        done: false,
        formData: undefined,
        phone: undefined,
        guid: undefined,
      }
      this.form = reduxForm(this.props.formConfig)(MainForm);
    }
    componentDidMount(){
      Api.fetchGuid().then(resp => {
        this.setState({guid: JSON.parse(resp.body)})
      });
    }
    handleSubmit = (formData) => {
      this.setState({formData: formData});
      this.makeSmsConfirmation();
    }
    handleSmsSubmit = (phone) => {
      this.setState(
        {phone: phone, smsChecking: false, smsCheckingDone: true},
      )
      this.submit();
    }
    makeSmsConfirmation = () => {
      if (!this.state.smsCheckingDone)
        return this.setState({smsChecking: true})
      return this.submit();
    }
    prepareFormData = () => {
      const { formConfig, prepareFormDataCb } = this.props;
      console.log(this.state.formData.toJS())
      const formData = this.state.formData.toJS();
      formData._csrf = window._csrf;
      formData[formConfig.formName].phone = this.state.phone;
      console.log('Form data prepared', this.state.phone, formData);
      return prepareFormDataCb(formData);
    }
    handleServerError = () => this.setState({
      submitted: true,
      hasErrors: true,
      loading: false
    })
    submit = () => {
      const {
        apiSubmitPromise,
        apiResponseOkHandleCb,
        apiResponseErrorHandleCb } = this.props;
      const formData = this.prepareFormData();
      this.setState({loading: true});
      // api submit promise must conform the signature
      // func (formData, errorHandlerCb)
      apiSubmitPromise(
        formData,
        this.handleServerError
      ).then(resp => {
        // api response must conform the belove structure
        // {status: Boolean, error: String, result: {request_id, token}}
        const respBody = JSON.parse(resp.body);
        if(resp.ok && respBody.status){
          this.setState({
            submitted: true,
            done: true,
            loading: false,
            result: respBody.result
          });
          apiResponseOkHandleCb(resp)
        } else {
          this.setState({
            submitted: true,
            hasErrors: true,
            errorMessage: respBody.error,
            loading: false
          });
          apiResponseErrorHandleCb(resp)
        }
      });
    }

    render() {
      const customStyle = {
        position: "relative",
        borderTop: "none"
      };
      const RegisteredForm = this.form;
      const { formConfig, authorityData } = this.props;
      const {
        loading, done, hasErrors, errorMessage,
        result, smsChecking, smsCheckingDone
      } = this.state;
      return (
        <div
          className="over-bottom"
          style={customStyle}
          >
            { loading ? <Spinner /> : null }
            { !done || hasErrors ?
              <span>
                { formConfig.showFormLabel &&
                  <h4 style={{color:"#00ac26"}}>
                    <FormattedMessage {...messages.formLabel} />
                  </h4>
                }
                { hasErrors ?
                  <div className="alert alert-danger">
                    {errorMessage || <FormattedMessage {...messages.formError} />}
                  </div> : null
                }
                <RegisteredForm
                  onSubmit={this.handleSubmit}
                  fileGuid={this.state.guid}
                  fileFieldName={(!!authorityData && 'user_file_org') || 'user_file'}
                  formName={formConfig.formName} />
              </span>
              : <SubmittedView authorityData={authorityData} code={result.request_id} token={result.token} />
            }
            {smsChecking && <SmsForm handleSmsCheck={this.handleSmsSubmit} done={smsCheckingDone} /> }
        </div>
      );

    }
  }
