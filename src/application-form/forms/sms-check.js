import React from 'react';
import { FormattedMessage } from 'react-intl';
import Modal from 'react-bootstrap/lib/Modal';
// import MaskedInput from 'react-text-mask';
import MaskedInput from 'react-maskedinput';


import {reshapePhone} from '../utils';
import getTranslations from 'Lib/get-translations';
import { Api } from '../api';

import messages from '../messages';


const translations = getTranslations();

export default class SmsForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      phone: (window.user && window.user.phone) || false,
      waiting: false,
      timedOut: false,
      incorrectCode: false,
      show: true,
      success: false
    };
  }
  componentWillReceiveProps(nextProps){
    if(!nextProps.done && !this.state.show)
      this.setState({show: true});
  }
  onHide = () => this.setState({show: false});
  handlePhoneClick = (phone) => {
    phone = reshapePhone(phone);
    Api.sendCode(phone).then(resp => {
      if(resp.ok && JSON.parse(resp.body).success){
        this.setState({
          phone, waiting: true
        });
      }
    });
  }
  handleCodeClick = (code) => {
    Api.checkCode(this.state.phone, code).then(resp => {
      const result = JSON.parse(resp.body);
      if(resp.ok && result.success){
        this.props.handleSmsCheck(this.state.phone);
      } else {
        this.setState({incorrectCode: true});
      }
    })
  }
  handleLateCodeClick = () => this.setState({timedOut: true, waiting: false});
  resendCode = () => this.setState({waiting: false, timedOut: false});
  render(){
    return (
      <span>
        <Modal show={this.state.show}
               onHide={this.onHide}
               bsSize="small"
               aria-labelledby="contained-modal-title-sm">
          <Modal.Header closeButton>
            <div id="contained-modal-title-sm">
              <b><FormattedMessage {...messages.showPhone} /></b>
            </div>
          </Modal.Header>
          <Modal.Body>
            {!this.state.success
              ? ((this.state.waiting && <CodeForm
                                          handleChange={this.handleCodeClick}
                                          handleTimeOut={this.handleLateCodeClick}
                                          error={this.state.incorrectCode}
                                          phone={this.state.phone} />)
                || (this.state.timedOut && <TimedOutView
                                            handleClick={this.resendCode}
                                            phone={this.state.phone} />)
                || <PhoneNumberForm handleClick={this.handlePhoneClick}
                                    phoneSettings={this.props.phoneSettings}
                                    phone={this.state.phone ? '+'+this.state.phone : false}/>)
              : null
            }
            {this.state.success
              ? <h1><FormattedMessage {...messages.phoneReal} /></h1>
              : null
            }
          </Modal.Body>
        </Modal>
      </span>
    )
  }
}

class PhoneNumberForm extends React.Component {
  constructor(props){
    super(props);
    let phone = null;
    if (this.props.phone) phone = this.props.phone;
    this.state = {
      error: (this.props.phone && false),
      hideButton: (this.props.phone && false),
      phone: phone,
      errorMsg: undefined
    }
  }
  validatePhone = (e) => {
    //
    const phone = e.target.value;
    // if(phone.length < 13) return;
    // const phonePattern = this.props.phoneSettings.pattern || /[+]998\d{2}-\d{3} \d{4}/;
    //
    // if(phonePattern.test(phone))
    this.setState({error: false, hideButton: false, phone: phone});
  }
  submitPhone = (e) => {
    this.setState({hideButton: true});
    !this.state.error && this.props.handleClick(this.state.phone)
  }
  render() {
    console.log("PHONE FORM", this.props.phoneSettings)
    const settings = this.props.phoneSettings || false;
    return (
      <div id="modalContent">
        <div className="send-sms" style={{display: "block"}}>

            <MaskedInput mask={(settings && settings.mask) || "+998111111111"}
                         type="text"
                         id="phone"
                         className="form-control"
                         name="phone"
                         value={this.state.phone}
                         placeholder={(settings && settings.pattern) || "+998xxxxxxxxx"}
                         onChange={this.validatePhone} />

            { this.state.errorMsg &&
              <div className="text-center alert-danger">
                <small>{this.state.errorMsg}</small>
              </div>
            }

            <div className="text-center">
              {this.state.hideButton
                ? <button id="sms-button"
                        className="btn btn-small btn-info waves-effect waves-light"
                        onClick={this.submitPhone}
                        style={{display: "inline-block"}}
                        disabled
                  >
                    <i className="glyphicon glyphicon-phone"></i>
                    <FormattedMessage {...messages.getCode} />
                </button> :
              <button id="sms-button"
                      className="btn btn-small btn-info waves-effect waves-light"
                      onClick={this.submitPhone}
                      style={{display: "inline-block"}}>
                  <i className="glyphicon glyphicon-phone"></i>
                  <FormattedMessage {...messages.getCode} />
              </button> }
            </div>
        </div>
      </div>
    );
  }
}

class CodeForm extends React.Component {
  state = {
    error: false,
    time: 0,
    progress: 0
  }
  componentDidMount(){
    this.timer = setInterval(this.handleClock, 1000);
  }
  componentWillUnmount(){
    clearTimeout(this.timer);
  }
  handleChange = () => {
    const val = this.input.value;
    if(val.length === 4 && /\d{4}/.test(val)){
      this.setState({error: false});
      this.props.handleChange(val);
    } else {
      this.setState({error: true});
    }
  }
  handleClock = () => {
    if(this.state.time === 60){
      this.props.handleTimeOut();
    }else{
      this.setState({
        time: this.state.time+1,
        progress: this.state.progress+1.667 // = 100 / 60
      });
    }
  }
  fixTimeFormat = (time) => {
    return (time < 10 && '0'+String(time)) || time;
  }
  render() {
    return (
      <div id="modalContent">
        <Alert phone={this.props.phone} />
        <div>
          <div className="col-md-10 no-padding">
              <div className="progress" style={{backgroundColor: '#d2d2d2'}}>
                  <div className="progress-bar bg-info"
                       role="progressbar"
                       style={{width: `${this.state.progress}%`}}
                       aria-valuenow="0"
                       aria-valuemin="0"
                       aria-valuemax="100" />
              </div>
          </div>

          <div className="col-md-2 no-padding">
              <div className="text-right" id="timecontainer">
                  <span className="afss_mins_bv">00</span>
                  <span className="afss_secs_point">:</span>
                  <span className="afss_secs_bv">
                    {this.fixTimeFormat(this.state.time)}
                  </span>
              </div>
          </div>
      </div>

      <input type="text"
             ref={(node) => this.input = node}
             onChange={this.handleChange}
             className="form-control"
             style={
               (this.state.error || this.props.error)
                ? {borderBottom: '1px solid #F44336', boxShadow: '0 1px 0 0 #F44336'}
                : null
             }
             name="confirm-code"
             placeholder={translations[messages.enterCode.id]} />

      </div>
    );
  }
}

const TimedOutView = ({handleClick, phone}) => (
  <div id="modalContent">
    <Alert phone={phone} />
    <p onClick={handleClick}>
        <span className="glyphicon glyphicon-repeat" aria-hidden="true"></span>
        <FormattedMessage {...messages.resendCode} />
    </p>
  </div>
)

const Alert = ({phone}) => (
  <div className="alertBox">
    <div id="w2" className="alert-success alert fade in">
        <FormattedMessage {...messages.codeSendTo} />: <strong id="user-number">+{phone}</strong>
    </div>
  </div>
);
