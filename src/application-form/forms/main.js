import React from 'react';
import {
  Field,
  FormSection
} from 'redux-form/immutable';
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';

import Entrepreneur from '../sections/entrepreneur';
import RegionSelector from '../sections/region-selector';
import { Api } from '../api';
import {
  SimpleInput,
  Textarea,
  Checkbox,
  MaskedInput,
  RadioButton,
  FileInput,
  makeSelectInput
} from '../field-components';
import {
  required,
  validateYear,
  validateEmail
} from '../validators';
// import getInitialUserData from './utils';
import messages from '../messages';


export default class Form extends React.Component {
  constructor(props){
    super(props);
    this.StatusSelectInput = makeSelectInput(Api.fetchStatus);
  }
  render(){
    const { handleSubmit, fileGuid, formName, fileFieldName } = this.props;
    return (
      <div className="send-appeal-form">
            <form className="card-block clearfix" onSubmit={handleSubmit}>

                <div className="col-md-6">
                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3">
                          <FormattedMessage {...messages.surname} />
                        </label>
                        <Field name={`${formName}[last_name]`}
                               validate={[required]}
                               component={SimpleInput}
                               type="text" />
                    </div>

                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3"
                          style={{marginTop: 5}}
                        >
                          <FormattedMessage {...messages.name} />
                        </label>
                        <Field name={`${formName}[first_name]`}
                               validate={[required]}
                               component={SimpleInput}
                               type="text" />
                    </div>

                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3"
                          style={{lineHeight: 'inherit', marginTop: 10}}
                        >
                          <FormattedMessage {...messages.secondName} />
                        </label>
                        <Field name={`${formName}[middle_name]`}
                               component={SimpleInput}
                               type="text" />
                    </div>

                    <FormSection>
                      <RegionSelector formName={formName} />
                    </FormSection>

                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3">
                          <FormattedMessage {...messages.address} />
                        </label>
                        <Field name={`${formName}[current_address]`} type="text"
                               component={SimpleInput}
                               validate={[required]}
                               forceEdit={true}
                               className="form-control" />
                    </div>

                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3">
                          <FormattedMessage {...messages.gender} />
                        </label>
                         <Field
                           labelStyle={{marginRight: "20px", marginLeft: "20px", lineHeight: "inherit"}}
                           name={`${formName}[gender]`}
                           validate={[required]}
                           component={RadioButton}
                           inputValue="m"
                           label={<FormattedMessage {...messages.man} />}
                         />
                         <Field
                           labelStyle={{marginRight: "20px", marginLeft: "20px", lineHeight: "inherit"}}
                           name={`${formName}[gender]`}
                           validate={[required]}
                           component={RadioButton}
                           inputValue="f"
                           label={<FormattedMessage {...messages.woman} />}
                         />
                    </div>

                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3">
                          <FormattedMessage {...messages.birthdate} />
                        </label>
                        <Field name={`${formName}[birthday]`} type="text"
                               component={MaskedInput}
                               mask="11.11.1111"
                               placeholder="dd.mm.yyyy"
                               validate={[required, validateYear]}
                               className="form-control" />
                    </div>

                </div>
                <div className="col-md-6">
                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3"
                          style={{lineHeight: "inherit", marginTop: 15}}
                        >
                          <FormattedMessage {...messages.email} />
                        </label>
                        <Field name={`${formName}[email]`} type="email"
                               component={SimpleInput}
                               validate={[required, validateEmail]}
                               placeholder="example@mail.com"
                               className="form-control" />
                    </div>

                    <div className="item clearfix">
                        <label className="form-label text-left col-md-3">
                          <FormattedMessage {...messages.status} />
                        </label>
                        <Field
                          name={`${formName}[employment_id]`}
                          validate={[required]}
                          component={this.StatusSelectInput}
                        />
                    </div>

                    <div className="item clearfix">
                        <label
                          className="form-label text-left col-md-3"
                          style={{lineHeight: "20px"}}>
                          <FormattedMessage {...messages.isSecret} />
                        </label>
                        <Field
                          labelStyle={{marginRight: "20px", marginLeft: "20px", lineHeight: "inherit"}}
                          name={`${formName}[is_public]`}
                          validate={[required]}
                          component={RadioButton}
                          inputValue={0}
                          label={<FormattedMessage {...messages.yes} />}
                        />
                        <Field
                          labelStyle={{marginRight: "20px", marginLeft: "20px", lineHeight: "inherit"}}
                          name={`${formName}[is_public]`}
                          validate={[required]}
                          component={RadioButton}
                          inputValue={1}
                          label={<FormattedMessage {...messages.no} />}
                        />
                        <div className="col-md-9 text-justify" style={{marginTop: '20px', paddingLeft: 25}} >
                          <p className="note"><FormattedMessage {...messages.secretInfo} /></p>
                        </div>
                    </div>

                    <FormSection>
                      <Entrepreneur formName={formName}/>
                    </FormSection>

                      <div className="item attachment-wrap clearfix" >
                           <div className="col-md-3" />
                           <div className="col-md-9" >
                              <Field
                                name={`${formName}[user_file]`}
                                component={FileInput}
                                fileGuid={fileGuid}
                                fileFieldName={fileFieldName}
                                type='hidden'
                              />
                           </div>
                      </div>
                </div>

                <div className="form-bottom col-md-12">
                    <div className="item aggrement-checkbox-wrap clearfix">

                    </div>
                    <div className="mgb-15 clearfix">
                        <label className="">
                          <FormattedMessage {...messages.applicationLabel} />
                        </label>
                        <Field name={`${formName}[description]`}
                               component={Textarea}
                               validate={[required]}
                              />
                        {/* <p className="note"><FormattedMessage {...messages.applicationHint} /></p> */}
                    </div>

                    <div className="row clearfix">
                        {/* <Field name="captcha" component={CaptchaField} validate={[required]}/> */}

                        <Field name={`${formName}[is_agree]`}
                               component={Checkbox}
                               onChange={(e) => {
                                 let elem = document.getElementById(`${formName}-offerModal-trigger`);
                                 e.target.checked && elem && elem.click();
                               }}
                               id={`${formName}-checkbox-aggrement`}
                               validate={[required]}
                               label={
                                 <FormattedHTMLMessage {...messages.agreementHint}
                                  values={{
                                    link: "#",
                                    id: `${formName}-offerModal-trigger`,
                                    dataToggle: "modal",
                                    dataTarget: "#offerModal"
                                  }} />
                               }
                               labelStyle={{
                                 marginBottom: 20
                               }}
                               type="checkbox" />

                        <div className="col-md-3" style={{marginTop: "5px"}}>
                          <button className="btn submit_btn pull-right">
                            <FormattedMessage {...messages.sendButton} />
                          </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    );
  }
}
