import { Api } from './api';
import messages from './messages';


// main validator
export const required = value => value ? undefined : "required";

export const asyncValidate = value => {
  // console.log(value, value.toJS().captcha);
  const val = value.toJS().captcha;
  return Api.checkCaptcha(val).then(
    (resp) => {
      if(!JSON.parse(resp.body).valid){
        // console.log('response', resp);
        return {captcha: messages.captchaError};
      }
    }
  );
}

export const validateFile = value => {
  if (value && value.size > 10485760) { // if more than 10 mb
    return messages.fileError;
  } else if (value && value.type === "application/x-msdownload"){ // something.exe
    return messages.fileError2;
  }
}

export const validatePhone = value => {
  const ptr = /^\+998(\d{9})$/g;
  return ptr.test(value) ? undefined : messages.phoneError;
}

export const validateYear = value => {
  const ptr = /^(0|1|2|3)\d{1}\.(1|0)\d{1}\.(19|20)\d{2}$/g;
  return ptr.test(value) ? undefined : messages.yearError;
}

export const validateEmail = value => {
  const ptr = /^[\w.-]+@\w+\.\w+$/g;
  return ptr.test(value) ? undefined : messages.emailError;
}
