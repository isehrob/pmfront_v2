import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field } from 'redux-form/immutable';

import messages from '../messages';
import {
  SimpleInput,
  Checkbox,
} from '../field-components/'
import { required } from '../validators';


export default class Entrepreneur extends React.Component {

  state = {checked: false};

  handleCheck = () => this.setState({checked: !this.state.checked});

  render() {
    const { formName } = this.props;
    return (
      <span>
        <div className="item business-checkbox-wrap clearfix">
            <label className="form-label text-left col-md-3"
              style={{marginTop: 15}}
            >
              <FormattedMessage {...messages.entrip} />
            </label>
            <Field name={`${formName}[is_juridical]`}
                   component={Checkbox}
                   onChange={this.handleCheck}
                   id={`${formName}-checkbox-business`}
                   label={<FormattedMessage {...messages.entripHint} />}
                   type="checkbox" />
        </div>
        { this.state.checked ?
          <div className="item clearfix">
              <label
                className="form-label text-left col-md-3"
                style={{lineHeight: "inherit", marginBottom: "5px"}}
              >
                <FormattedMessage {...messages.entripName} />
              </label>
              <Field name={`${formName}[juridical_name]`}
                     validate={[required]}
                     component={SimpleInput}
                     type="text" />
          </div> : null
        }
      </span>
    )
  }
}
