import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field } from 'redux-form/immutable';

import messages from '../messages';
import {
  SelectInput,
} from '../field-components/'
import { required } from '../validators';
import { Api } from '../api';


export default class RegionSelector extends React.Component {
  state = {
    regions: [],
    districts: [],
    makhallas: [],
    currentRegion: undefined
  }
  updateDistricts = (e) => {
    const regionId = e.target.value;
    Api.fetchDistricts(regionId).then(resp => {
      this.setState({districts: JSON.parse(resp.body), currentRegion: regionId});
    });
  }
  updateMakhallas = (e) => {
    const districtId = e.target.value;
    Api.fetchMakhallas(this.state.currentRegion, districtId).then(resp => {
      this.setState({makhallas: JSON.parse(resp.body)});
    });
  }
  componentDidMount(){
    Api.fetchRegions().then(resp => {
      console.log('got regions', resp);
      this.setState({regions: JSON.parse(resp.body)});
    });
  }
  render() {
    const { regions, districts, makhallas } = this.state;
    const { formName } = this.props;
    return (
      <span>
        <div className="item clearfix">
            <label className="form-label text-left col-md-3">
              <FormattedMessage {...messages.region} />
            </label>
            <Field name={`${formName}[region_id]`} type="text"
                   component={SelectInput}
                   validate={[required]}
                   onChange={this.updateDistricts}
            >
              <option />
              {Object.keys(regions).map(
                key => <option key={key} value={key}>{regions[key]}</option>
              )}
            </Field>
        </div>

        <div className="item clearfix">
            <label className="form-label text-left col-md-3"
              style={{marginTop: 5}}
            >
              <FormattedMessage {...messages.district} />
            </label>
            <Field name={`${formName}[district_id]`} type="text"
                   component={SelectInput}
                   validate={[required]}
                   onChange={this.updateMakhallas}
            >
              <option />
              {Object.keys(districts).map(
                key => <option key={key} value={key}>{districts[key]}</option>
              )}
            </Field>
        </div>

        <div className="item clearfix">
            <label className="form-label text-left col-md-3"
              style={{marginTop: 5}}
            >
              <FormattedMessage {...messages.makhalla} />
            </label>
            <Field name={`${formName}[mahalla_id]`} type="text"
                   component={SelectInput}
            >
              <option />
              {Object.keys(makhallas).map(
                key => <option key={key} value={key}>{makhallas[key]}</option>
              )}
            </Field>
        </div>
      </span>
    );
  }
}
