import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Field } from 'redux-form/immutable';

import messages from '../messages';
import {
  SelectInput,
} from '../field-components/'
import { required } from '../validators';
import { Api } from '../api';


export default class CountrySelector extends React.Component {
  state = {
    countries: [],
    currentCountry: undefined
  }
  componentDidMount(){
    Api.fetchCountries().then(resp => {
      console.log('got countries', resp);
      this.setState({countries: JSON.parse(resp.body)});
    });
  }
  render() {
    const { countries } = this.state;
    console.log('countires', countries);
    const { formName, handleCountryChange } = this.props;
    return (
      <div className="item clearfix">
          <label className="form-label text-left col-md-3">
            <FormattedMessage {...messages.country} />
          </label>
          <Field name={`${formName}[country_id]`} type="text"
                 component={SelectInput}
                 onChange={handleCountryChange}
                 validate={[required]}
          >

            {[
                <option />,
                ...Object.keys(countries).map(
                  key => <option key={key} value={key}>{ countries[key] }</option>
                )
            ]}
          </Field>
      </div>
    );
  }
}
