import React from 'react';
import { FormattedMessage } from 'react-intl';
import Dropzone from 'react-dropzone';

import messages from '../messages';
import { Api } from '../api';


export default class FileField extends React.Component {
    constructor(props){
      super(props);
      this.state = {
        error: false,
        errorMessage: <FormattedMessage {...messages.notAccepted} />,
        files: []
      }
    }
    componentDidMount() {
      let fileFieldName = this.props.fileFieldName;
      this.params = {
        _guid: this.props.fileGuid,
        _field: 76,
        _name: fileFieldName
      };
      this.params[fileFieldName] = null;
    }
    _accepted = () => {
      return `image/jpeg, image/png, application/pdf, \
              application/vnd.openxmlformats-officedocument.wordprocessingml.document \
              application/vnd.openxmlformats-officedocument.spreadsheetml.sheet \
              application/vnd.ms-excel, text/plain, application/zip \
              application/x-rar-compressed, application/x-rar, \
              application/octet-stream,application/zip, \
              compressed/rar,application/rar,application/x-compressed, \
              multipart/x-zip,multipart/x-rar
              `;
    }
    onDrop = (files, rejected) => {
      if (this.state.files.length + files.length > 9) return this.rejectMaxNumber();
      if (rejected.length > 0) return this.rejectedDropped();
      files.forEach(file => {
        this.uploadToServer(file);
        this.props.input.onChange(this.props.fileGuid);
      });
    };
    rejectedDropped = () => {
      this.setState({
        error: true,
        errorMessage: <FormattedMessage {...messages.notAccepted} />
      });
    }
    rejectMaxNumber = () => {
      this.setState({
        error: true,
        errorMessage: <FormattedMessage {...messages.maxNumberOverflow} />
      });
    }
    uploadToServer = (file) => {
      const fileFieldName = this.props.fileFieldName;
      this.params[fileFieldName] = file.name
      this.params['_guid'] = this.props.fileGuid;
      Api.uploadFile(
        file,
        this.params,
        () => this.setState({error: true})
      ).then(
          (resp) => {
            const respBody = JSON.parse(resp.text);
            if(resp.ok && respBody.success){
              this.setState({
                files: [respBody.label, ...this.state.files],
                error: false
              });
            } else {
              this.setState({error: true});
            }
          }
      );
    }
    removeUploadedFile = (file) => {
      this.params.user_file_org = file;
      Api.deleteUploadedFile(this.params).then(
          (resp) => {
            const respBody = JSON.parse(resp.text);
            if(resp.ok && respBody.success){
              let files = this.state.files.filter(fl => fl !== file);
              this.setState({error: false, files: files});
            } else {
              this.setState({error: true});
            }
          }
      );
    }
    render() {
      return (
        <span>
          <Dropzone style={{border: 0}}
                    onDrop={this.onDrop}
                    maxSize={10485760}
          >
            <div id="user_file_org_box" className="fileUploaderBox">
              <div id="user_file_org" className="btn btn-primary waves-effect waves-light">
                <i className="icon-file icon-white"></i><FormattedMessage {...messages.uploadFile} />
              </div>
              <div className="file-path-wrapper" style={{display: "inline-block"}}>
                  <input readOnly="" id="filename" className="file-path" type="text" />
              </div>
              <div className="col-md-12 text-justify">
                <p className="note"><FormattedMessage {...messages.fileSize} /></p>
              </div>
              <div className="clearfix"></div>
            </div>
          </Dropzone>
          <div className="dropZone fileUploaderFilesListBox">
            <ul id="user_fileFilesList"
                className="fileUploaderFilesList"
                style={{border: 0, margin: 0, padding: 0}}>
                {this.state.files.map(file => (
                  <li className="fuPill fu-Success" key={file}>
                    <a className="cancelBtn deleteButton"
                       onClick={() => this.removeUploadedFile(file)}
                       style={{width: 'auto'}}>
                        <span>×</span>
                    </a>
                    <div className="filesListBoxIcon pull_left">
                        <i className="fu_icon fu_png"></i>
                    </div>
                    <p className="fileUploaderFileName">{file}</p>
                    <p className="fuMessage"><FormattedMessage {...messages.fileUploaded} /></p>
                  </li>
                ))}
                {this.state.error
                  ?  <li className="fuPill fu-Error" key="error">
                        <div className="filesListBoxIcon pull_left">
                            <i className="fu_icon fu_png"></i>
                        </div>
                        <p className="fileUploaderFileName" style={{color: 'red'}}>
                          <FormattedMessage {...messages.error} />
                        </p>
                        <p className="fuMessage" style={{color: 'red'}}>
                          {this.state.errorMessage}
                        </p>
                      </li>
                  : null
                }

            </ul>
          </div>
        </span>
      );
    }
}
