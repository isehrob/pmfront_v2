import React from 'react';


export const SelectInput = ({
  input,
  label,
  children,
  meta: { touched, error, warning }
}) => {
  return (
    <div className="col-md-9">
        <div className="md-form">
            <select className={(touched && error && "select-error") || undefined} {...input}>
                {children}
            </select>
        </div>
    </div>
  );
}

// generates select input with the given api as options' source
export const makeSelectInput = (api) =>
  class extends React.Component {
    state = {data: []};
    componentDidMount(){
      api().then((resp) => {this.setState({data: resp})});
    }
    render() {
      // eslint-disable-next-line no-unused-vars
      const { input, meta: { touched, error, warning }} = this.props;
      const { data } = this.state;
      return (
        <div className="col-md-9">
            <div className="md-form">
                <select {...input}
                        className={(touched && error && "select-error") || undefined}>
                  <option />
                  { data.map(item =>
                      <option key={item.id} value={item.id}>
                        {item.value}
                      </option>) }
                </select>
            </div>
        </div>
      );
    }
  }

  export default SelectInput;
