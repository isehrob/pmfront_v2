import React from 'react';


export default class RadioButton extends React.Component {
  handleClick = () => {
    const {initial, dirty} = this.props.meta;
    if (initial && !dirty) return;
    this.input.checked = true
    this.props.input.onChange(this.input.value);
  }
  componentDidMount(){
    if(this.props.inputValue === this.props.input.value){
      this.input.checked = true;
    }
  }
  render() {
    const {
      input, label, id, labelStyle, inputValue,
      meta: { touched, error, initial, dirty }
    } = this.props;

    return (
      <div
        className="pull-left"
      >
          <div className="md-form">
              <span key={label}>
                <input {...input}
                  ref={(node) => (this.input = node) }
                  type="radio"
                  value={inputValue}
                  disabled={initial && !dirty}
                  id={id}/>
                <label htmlFor={id}
                       onClick={this.handleClick}
                       style={{color: (touched && error && "red") || '#000', ...labelStyle}}
                      >{label}</label>
              </span>
          </div>
      </div>
    );
  }
}
