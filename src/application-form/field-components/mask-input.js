import React from 'react';
// import ReactTextMask from 'react-text-mask';
import MaskedInput from 'react-maskedinput';
import { FormattedMessage } from 'react-intl';


export default ({
  input, label, type, mask, id, placeholder,
  meta: { touched, error, warning, initial, dirty }
}) => (
  <div className="col-md-9">
      <div className="md-form">
          <MaskedInput {...input}
            disabled={initial && !dirty}
            mask={mask}
            placeholder={placeholder}
            className={(touched && error && "input-error") || undefined}
            type="text"
            id={id}
          />
          { touched
            && ((error
                  && error !== "required"
                  && <span style={{color: "red", marginTop: 5}}>
                      <FormattedMessage {...error} />
                    </span>
                  ))
          }
      </div>
  </div>
);
