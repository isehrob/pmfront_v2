import React from 'react';
import { FormattedMessage } from 'react-intl';


export default ({
  input,
  label,
  type,
  placeholder,
  forceEdit = false,
  meta: { touched, error, warning, initial, dirty }
}) => {
  let disabled;
  if(forceEdit){
    disabled = false;
  } else {
    disabled = initial && !dirty;
  }
  return (
    <div className="col-md-9">
        <div className="md-form">
            <input {...input}
                   placeholder={placeholder}
                   type={type}
                   className={(touched && error && "input-error") || undefined}
                   disabled={disabled}
            />
            { touched
              && ((error
                    && error !== "required"
                    && <span style={{color: "red", marginTop: 5}}>
                        <FormattedMessage {...error} />
                      </span>
                    ))
            }
        </div>
    </div>
  );
}
