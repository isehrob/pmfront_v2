import React from 'react';


export default ({
  input,
  label,
  type,
  meta: { touched, error, warning }
}) => (
  <div>
    <textarea {...input}
      className={(touched && error && "textarea-error") || undefined} />
  </div>
);
