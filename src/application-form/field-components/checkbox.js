import React from 'react';


export default ({
  input, label, labelStyle, type, id, wide,
  meta: { touched, error, warning, initial, dirty }
}) => {
  const color = (touched && error && "red") || '#000';
  console.log('LKDJFLKSJDLKF', color)
  return (
    <div
      className={(wide && "col-md-12") || "col-md-9"}
      style={wide && {textAlign: "center"}}
    >
        <div className="md-form">
            <input type="checkbox" {...input} id={id} disabled={initial && dirty} />
            <label htmlFor={id}
                   style={{color, ...labelStyle}}
              >{label}</label>
        </div>
    </div>
  );
}
