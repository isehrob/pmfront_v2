import {delay} from 'Common/utils';


export const fetchCaptcha = (resolve) => {
  console.log('in captcha');
  return delay(500).then(
    () => new Promise(
      () => resolve(
        false,
        {
          ok: true,
          body: JSON.stringify({src:"/files/captcha-example.png"})
        }
      )
    )
  );
}

export const checkCaptcha = (val) => {
  console.log('in captcha');
  return delay(500).then(
    () => new Promise(
      resolve => resolve({body: JSON.stringify({valid: false})})
    )
  );
}

export const fetchGender = () => {
  return delay(500).then(
    () => {
      return [{id: 1, value: 'Эркак'} ,{id: 2, value: 'Аёл'}];
    }
  );
}

export const fetchPrivacy = () => {
  return delay(500).then(
    () => {
      return [{id: 1, value: 'Ҳа'} ,{id: 2, value: 'Йўқ'}];
    }
  );
}

export const fetchStatus = () => {
  return delay(500).then(
    () => {
      return [
        {id: 1, value: 'Банд-ишлайди'},
        {id: 2, value: 'Ишсиз'},
        {id: 3, value: 'Нафақахўр'},
        {id: 4, value: 'Талаба'}
      ];
    }
  );
}

export const fetchRegions = () => {
  return delay(500).then(
    () => {
      return {body: JSON.stringify({1: 'Toshkent', 2: 'Boshqakent'})};
    }
  );
}

export const fetchDistricts = (regionId) => {
  console.log('got regionId', regionId)
  return delay(500).then(
    () => {
      return {body: JSON.stringify({1: 'Jidazor', 2: 'Uzumzor'})};
    }
  );
}

export const fetchGuid = () => {
  return delay(500).then(
    () => {
      return {body:"slkdjfksljfdslkjfddslkfjsf"};
    }
  );
}

export const fetchPhonePattern = (id) => {
  return delay(500).then(
    () => {
      return {body: JSON.stringify({
        pattern: "+720000000",
        mask: "+721111111"
      })};
    }
  );
}

export const fetchCountries = () => {
  return delay(500).then(
    () => {
      return {body: JSON.stringify({1: 'Ukraina', 2: 'USA'})};
    }
  );
}

// new endpoints
export const fetchMakhallas = (regionId, districtId) => {
  console.log('got regionId', regionId, districtId)
  return delay(500).then(
    () => {
      return {body: JSON.stringify({1: 'Bodomzor', 2: 'Giloszor'})};
    }
  );
}

export const uploadFile = (file, params) => {
  return delay(500).then(
    () => {
      return {
        ok: true,
        text: JSON.stringify({
          guid:"9feb4506-b7d4-ce82-29cc-2efe0b17303d",
          label: "21463066_1556832284338260_8683481151752919937_n.jpg",
          success: true
        })
      };
    }
  );
}

export const deleteUploadedFile = (params) => {
  return delay(500).then(
    () => {
      return {
        ok: true,
        text: JSON.stringify({
          guid: null,
          success: true
        })
      };
    }
  );
}

export const sendCode = (phone) => {
  return delay(500).then(
    () => {
      return {
        ok: true,
        body: JSON.stringify({'success': true})
      };
    }
  );
}

export const checkCode = (phone, code) => {
  return delay(500).then(
    () => {
      return {
        ok: true,
        body: JSON.stringify({success: true})
      };
    }
  );
}
