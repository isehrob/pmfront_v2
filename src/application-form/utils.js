if(process.env.NODE_ENV !== "production") {
	window.user = {
		"first_name":"SEHROB",
		"last_name":"IBROHIMOV",
		"middle_name":"MAXMADIYOROVICH",
		"email":"isehrob@gmail.com",
		"current_address":"Кашадарья, Чирокчи р-н, Тарагай",
		"gender":"m",
		"birthday":"1990-07-10",
		"recipient_type_id":"",
		"juridical_name":"",
		"pin_fl":"31007902700052",
		"phone": "998935918467"
	}
}

export const reshapeYear = (year) => {
	let yearList = year.split('-');
	return yearList[2]+'.'+yearList[1]+'.'+yearList[0];
}

export const reshapeGender = (gender) => gender.toLowerCase();

export const reshapePhone = (phone) => phone.replace(/[+\s-]/g, '');

export const reshapeAddress = (user) => {
	return user.current_address;
};

export default (formName) => {
  if(!window.user) return null;
  const resultObject = {};
	resultObject[formName] = {
			...window.user,
			birthday: reshapeYear(window.user.birthday),
			gender: reshapeGender(window.user.gender),
			// current_address: reshapeAddress(window.user)
  };
	return resultObject;
}
