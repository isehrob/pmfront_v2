import React from 'react';
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';

import messages from './messages';

import services from 'Lib/services';

const translations = services.translationService();


export default ({code, token, authorityData}) => (
  <div className="col-md-12">
      {(authorityData &&
        <h3 className="text-success">
          <FormattedHTMLMessage {...messages.applicationSuccess}
              values={{title: authorityData.title}}/>
        </h3>) ||
        <h3 className="text-success">
          <FormattedHTMLMessage {...messages.applicationSuccess}
              values={{title: translations["apps.mainFormTitle"] }}/>
        </h3>
      }
      <div className="panel panel-success request-sent">
          <div className="panel-heading">
              <div className="panel-title"><FormattedMessage {...messages.pleaseSave} /></div>
          </div>
          <div className="panel-body">
              <table id="print" className="table table-bordered table-hover table-bordered">
                  <tbody>
                    <tr>
                      <td><FormattedMessage {...messages.applicationNo} /></td>
                      <td>{code}</td>
                    </tr>
                    <tr>
                      <td><FormattedMessage {...messages.applicationToken} /></td>
                      <td>{token}</td>
                    </tr>
                  </tbody>
              </table>
              {/* <button id="print" className="btn btn-primary pull-right" onClick="printContent('print')">
                  <span className="glyphicon glyphicon-print" aria-hidden="true"></span>
                  <FormattedMessage {...messages.printButton} />
              </button> */}
          </div>
      </div>
  </div>
)
