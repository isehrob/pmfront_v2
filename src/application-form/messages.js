/*
 * PersonalWidgets Messages
 *
 * This contains all the text for the PersonalWidgets component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  entripName: {
    id: "application_form.entripName",
    defaultMessage: "Тадбиркорлик субъекти номи"
  },
  formError: {
    id: "application_form.formError",
    defaultMessage: "Хизматдан фойдаланиб бўлмади. Кейинроқ уриниб кўринг."
  },
  phoneError: {
    id: "application_form.phoneError",
    defaultMessage: "Телефон рақамини тўғри киритинг."
  },
  phoneLabel: {
    id: "application_form.phoneLabel",
    defaultMessage: "Телефон (+998xxxxxxxxx)"
  },
  agreementHint: {
    id: "application_form.agreementHint",
    defaultMessage: "Фойдаланиш <a href={link} id={id} data-toggle={dataToggle} data-target={dataTarget}>қоидалари</a> билан танишдим",
  },
  yearError: {
    id: "application_form.yearError",
    defaultMessage: "Тўғилган йилингизни тўғри форматда киритинг: (dd.mm.19xx/20xx)"
  },
  emailError: {
    id: "application_form.emailError",
    defaultMessage: "Тўғри электрон почта манзилини кўрсатинг"
  },
  surname: {
    id: "application_form.surname",
    defaultMessage: "Фамилия"
  },
  name: {
    id: "application_form.name",
    defaultMessage: "Исм"
  },
  secondName: {
    id: "application_form.secondName",
    defaultMessage: "Отасининг исми"
  },
  fileError: {
  	id: "application_form.fileError",
  	defaultMessage: "5 мбдан катта файл юклаш мумкин эмас!"
  },
  fileError2: {
    id: "application_form.fileError2",
    defaultMessage: "Бундай форматдаги файлни юклаш мумкин эмас!"
  },
  captchaError: {
  	id: "application_form.captchaError",
  	defaultMessage: "Жавоб нотўғри. Бошқаттан уриниб кўринг."
  },
  captchaHint: {
  	id: "application_form.captchaHint",
  	defaultMessage: "Расмдаги рақамларни киритинг"
  },
  region: {
  	id: "application_form.region",
  	defaultMessage: "Ҳудуд"
  },
  district: {
  	id: "application_form.district",
  	defaultMessage: "Туман (шаҳар)"
  },
  country: {
    id: "application_form.country",
    defaultMessage: "Давлат"
  },
  gender: {
  	id: "application_form.gender",
  	defaultMessage: "Жинс"
  },
  birthdate: {
  	id: "application_form.birthdate",
  	defaultMessage: "Туғилган сана"
  },
  isSecret: {
  	id: "application_form.isSecret",
  	defaultMessage: "Мурожаат ошкоралиги"
  },
  address: {
  	id: "application_form.address",
  	defaultMessage: "Манзил"
  },
  email: {
  	id: "application_form.email",
  	defaultMessage: "Электрон почта"
  },
  phone: {
  	id: "application_form.phone",
  	defaultMessage: "Телефон"
  },
  entrip: {
  	id: "application_form.entrip",
  	defaultMessage: "Тадбиркор"
  },
  entripHint: {
  	id: "application_form.entripHint",
  	defaultMessage: "Тадбиркорлик субъекти сифатида мурожаат қилаётган бўлсангиз ушбу катакчани белгиланг"
  },
  status: {
  	id: "application_form.status",
  	defaultMessage: "Мақом"
  },
  fileLabel: {
  	id: "application_form.fileLabel",
  	defaultMessage: "Қўшимча файл"
  },
  fileHint: {
  	id: "application_form.fileHint",
  	defaultMessage: "Қўшимча файлни кўрсатинг <br/>(хажми  5мб гача)"
  },
  applicationLabel: {
  	id: "application_form.applicationLabel",
  	defaultMessage: "Мурожаат матни"
  },
  applicationHint: {
  	id: "application_form.applicationHint",
  	defaultMessage: "Мурожаатингиз тезкор равишда ҳал этилишини истасангиз, ҳар бир масала бўйича алоҳида мурожаат йўллашингизни ҳамда мазкур мурожаатни лўнда, оддий ва равон тилда баён этишингизни сўраймиз!"
  },
  sendButton: {
  	id: "application_form.sendButton",
  	defaultMessage: "Жўнатиш"
  },
  notAccepted: {
    id: "application_form.notAccepted",
    defaultMessage: "Тақиқланган файл"
  },
  maxNumberOverflow:{
    id: "application_form.maxNumberOverflow",
    defaultMessage: "Жами файллар сони 10 тадан ошмаслиги лозим"
  },
  uploadFile: {
    id: "application_form.uploadFile",
    defaultMessage: "Файлни юкланг"
  },
  phoneReal: {
    id: "application_form.phoneReal",
    defaultMessage: "Telefon raqami tasdiqlandi!"
  },
  getCode: {
    id: "application_form.getCode",
    defaultMessage: "Kod olish"
  },
  enterCode: {
    id: "application_form.enterCode",
    defaultMessage: "Tekshiruv kodini kiriting"
  },
  resendCode: {
    id: "application_form.resendCode",
    defaultMessage: "Qayta yuborish"
  },
  codeSendTo: {
    id: "application_form.codeSendTo",
    defaultMessage: "Tekshiruv kodi ushbu raqamga yuborildi"
  },
  fileSize: {
    id: "application_form.fileSize",
    defaultMessage: "Файлнинг ҳажми 10 мб дан ва жами файллар сони 10 тадан ошмаслиги лозим"
  },
  fileUploaded: {
    id: "application_form.fileUploaded",
    defaultMessage: "Файл юкланди"
  },
  error: {
    id: "application_form.error",
    defaultMessage: "Хато!"
  },
  secretInfo: {
    id: "application_form.secretInfo",
    defaultMessage: "Мурожаат ва мурожаатчи тўғрисида маълумотларнинг ошкоралиги"
  },
  makhalla: {
    id: "application_form.makhalla",
    defaultMessage: "Маҳалла"
  },
  man: {
    id: "application_form.man",
    defaultMessage: "Эркак"
  },
  woman: {
    id: "application_form.woman",
    defaultMessage: "Аёл"
  },
  yes: {
    id: "application_form.yes",
    defaultMessage: "Ҳа"
  },
  no: {
    id: "application_form.no",
    defaultMessage: "Йўқ"
  },
  showPhone: {
    id: "application_form.showPhone",
    defaultMessage: "Murojaat yo'llash uchun telefon raqamingizni kiriting."
  },
  unknownError:{
    id: "application_form.unknownError",
    defaultMessage: "Хатолик юз берди!"
  },
  formLabel: {
    id: "application_form.formLabel",
    defaultMessage: "Мурожаат юбориш"
  },
  applicationSuccess: {
    id: "application_form.applicationSuccess",
    defaultMessage: `Сизнинг мурожаатингиз {title}га муваффақиятли юборилди!`
  },
  pleaseSave: {
    id: "application_form.pleaseSave",
    defaultMessage: "Диққат!!! Қуйидаги малумотларни сақлаб олинг"
  },
  applicationNo: {
    id: "application_form.applicationNo",
    defaultMessage: `Мурожаат рақами`
  },
  applicationToken: {
    id: "application_form.applicationToken",
    defaultMessage: `Текшириш коди`
  },
});
